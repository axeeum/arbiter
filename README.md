# arbiter

## dependencies
+ install [brew](http://brew.sh/)
+ install [java](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
+ install [yarn](https://yarnpkg.com)
+ install [ruby](https://www.ruby-lang.org/en/)
  + [using homebrew](https://www.ruby-lang.org/en/documentation/installation/#homebrew)
  + [using rbenv](https://github.com/rbenv/rbenv)
+ install [bundler]()

### first time
```
gem install bundler
bundle install
```

### api
```
./gradlew applications:arbiter-api:bootrun
```

### ui
```
./gradlew applications:arbiter-ui:bootrun
```

### development
```
foreman start
```

### testing
_all tests_
```
rake
```

_acceptance_
```
cd acceptance
bundle install
bundle exec rspec
```

_api unit tests_
```
./gradlew applications:arbiter-api:test
```

_ui java unit tests_
```
./gradlew applications:arbiter-ui:test
```

_ui javascript unit tests_
```
cd applications/arbiter-ui
yarn test
```
