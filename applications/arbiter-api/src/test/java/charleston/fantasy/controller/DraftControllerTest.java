package charleston.fantasy.controller;

import charleston.fantasy.model.ArbitrateDraftRequest;
import charleston.fantasy.model.DraftBoard;
import charleston.fantasy.model.LeagueMember;
import charleston.fantasy.service.DraftBoardService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static charleston.fantasy.model.SelectionBuilder.selectionBuilder;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DraftControllerTest {

    @Mock
    private DraftBoardService draftBoardService;

    private DraftController draftController;

    @Before
    public void setUp() throws Exception {
        draftController = new DraftController(draftBoardService);
    }

    @Test
    public void arbitrate_returnsDraftBoard()  {
        LeagueMember member = new LeagueMember(1, "a", Optional.empty(), Optional.empty());
        ArbitrateDraftRequest request = new ArbitrateDraftRequest(1, singletonList(member));

        DraftBoard expected = new DraftBoard(singletonList(selectionBuilder().round(1).leagueMember(member).build()));
        when(draftBoardService.arbitrate(request.getLeagueMembers(), request.getRounds())).thenReturn(expected);

        DraftBoard actual = draftController.draft(request);
        verify(draftBoardService).arbitrate(request.getLeagueMembers(), request.getRounds());
        assertThat(actual, equalTo(expected));
    }
}