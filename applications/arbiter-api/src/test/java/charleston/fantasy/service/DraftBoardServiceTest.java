package charleston.fantasy.service;

import charleston.fantasy.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static charleston.fantasy.model.KeeperBuilder.keeperBuilder;
import static charleston.fantasy.model.LeagueMemberBuilder.leagueMemberBuilder;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(MockitoJUnitRunner.class)
public class DraftBoardServiceTest {

    private DraftBoardService sut;
    private ArbitrateDraftRequest rqst;

    @Before
    public void setup() {
        rqst = new ArbitrateDraftRequest(
            8,
            asList(
                leagueMemberBuilder().name("a").build(),
                leagueMemberBuilder().name("b").build(),
                leagueMemberBuilder().name("c").build(),
                leagueMemberBuilder().name("d").build(),
                leagueMemberBuilder().name("e").build(),
                leagueMemberBuilder().name("f").build(),
                leagueMemberBuilder().name("g").build(),
                leagueMemberBuilder().name("h").build()
            )
        );

        KeeperHelper keeper = new KeeperHelper();
        sut = new DraftBoardService(new FlowHelper(keeper), keeper);
    }

    @Test
    public void arbitrate_returnsDraftBoard() {
        DraftBoard actual = sut.arbitrate(rqst.getLeagueMembers(), rqst.getRounds());

        assertThat(actual.getSelections().size(), equalTo(64));
        assertThat(actual.getSelections().get(0).getLeagueMember().getName(), equalTo("a"));
        assertThat(actual.getSelections().get(63).getLeagueMember().getName(), equalTo("a"));
    }

    @Test
    public void arbitrate_snakeOrdersDraftBoard_noKeepers() {
        List<Selection> selections = asList(
            new Selection(1, leagueMemberBuilder().name("a").build(), Optional.empty(), 1),
            new Selection(1, leagueMemberBuilder().name("b").build(), Optional.empty(), 2),
            new Selection(1, leagueMemberBuilder().name("c").build(), Optional.empty(), 3),
            new Selection(1, leagueMemberBuilder().name("d").build(), Optional.empty(), 4),
            new Selection(1, leagueMemberBuilder().name("e").build(), Optional.empty(), 5),
            new Selection(1, leagueMemberBuilder().name("f").build(), Optional.empty(), 6),
            new Selection(1, leagueMemberBuilder().name("g").build(), Optional.empty(), 7),
            new Selection(1, leagueMemberBuilder().name("h").build(), Optional.empty(), 8),
            new Selection(2, leagueMemberBuilder().name("h").build(), Optional.empty(), 9),
            new Selection(2, leagueMemberBuilder().name("g").build(), Optional.empty(), 10),
            new Selection(2, leagueMemberBuilder().name("f").build(), Optional.empty(), 11),
            new Selection(2, leagueMemberBuilder().name("e").build(), Optional.empty(), 12),
            new Selection(2, leagueMemberBuilder().name("d").build(), Optional.empty(), 13),
            new Selection(2, leagueMemberBuilder().name("c").build(), Optional.empty(), 14),
            new Selection(2, leagueMemberBuilder().name("b").build(), Optional.empty(), 15),
            new Selection(2, leagueMemberBuilder().name("a").build(), Optional.empty(), 16),
            new Selection(3, leagueMemberBuilder().name("a").build(), Optional.empty(), 17),
            new Selection(3, leagueMemberBuilder().name("b").build(), Optional.empty(), 18),
            new Selection(3, leagueMemberBuilder().name("c").build(), Optional.empty(), 19),
            new Selection(3, leagueMemberBuilder().name("d").build(), Optional.empty(), 20),
            new Selection(3, leagueMemberBuilder().name("e").build(), Optional.empty(), 21),
            new Selection(3, leagueMemberBuilder().name("f").build(), Optional.empty(), 22),
            new Selection(3, leagueMemberBuilder().name("g").build(), Optional.empty(), 23),
            new Selection(3, leagueMemberBuilder().name("h").build(), Optional.empty(), 24),
            new Selection(4, leagueMemberBuilder().name("h").build(), Optional.empty(), 25),
            new Selection(4, leagueMemberBuilder().name("g").build(), Optional.empty(), 26),
            new Selection(4, leagueMemberBuilder().name("f").build(), Optional.empty(), 27),
            new Selection(4, leagueMemberBuilder().name("e").build(), Optional.empty(), 28),
            new Selection(4, leagueMemberBuilder().name("d").build(), Optional.empty(), 29),
            new Selection(4, leagueMemberBuilder().name("c").build(), Optional.empty(), 30),
            new Selection(4, leagueMemberBuilder().name("b").build(), Optional.empty(), 31),
            new Selection(4, leagueMemberBuilder().name("a").build(), Optional.empty(), 32),
            new Selection(5, leagueMemberBuilder().name("a").build(), Optional.empty(), 33),
            new Selection(5, leagueMemberBuilder().name("b").build(), Optional.empty(), 34),
            new Selection(5, leagueMemberBuilder().name("c").build(), Optional.empty(), 35),
            new Selection(5, leagueMemberBuilder().name("d").build(), Optional.empty(), 36),
            new Selection(5, leagueMemberBuilder().name("e").build(), Optional.empty(), 37),
            new Selection(5, leagueMemberBuilder().name("f").build(), Optional.empty(), 38),
            new Selection(5, leagueMemberBuilder().name("g").build(), Optional.empty(), 39),
            new Selection(5, leagueMemberBuilder().name("h").build(), Optional.empty(), 40),
            new Selection(6, leagueMemberBuilder().name("h").build(), Optional.empty(), 41),
            new Selection(6, leagueMemberBuilder().name("g").build(), Optional.empty(), 42),
            new Selection(6, leagueMemberBuilder().name("f").build(), Optional.empty(), 43),
            new Selection(6, leagueMemberBuilder().name("e").build(), Optional.empty(), 44),
            new Selection(6, leagueMemberBuilder().name("d").build(), Optional.empty(), 45),
            new Selection(6, leagueMemberBuilder().name("c").build(), Optional.empty(), 46),
            new Selection(6, leagueMemberBuilder().name("b").build(), Optional.empty(), 47),
            new Selection(6, leagueMemberBuilder().name("a").build(), Optional.empty(), 48),
            new Selection(7, leagueMemberBuilder().name("a").build(), Optional.empty(), 49),
            new Selection(7, leagueMemberBuilder().name("b").build(), Optional.empty(), 50),
            new Selection(7, leagueMemberBuilder().name("c").build(), Optional.empty(), 51),
            new Selection(7, leagueMemberBuilder().name("d").build(), Optional.empty(), 52),
            new Selection(7, leagueMemberBuilder().name("e").build(), Optional.empty(), 53),
            new Selection(7, leagueMemberBuilder().name("f").build(), Optional.empty(), 54),
            new Selection(7, leagueMemberBuilder().name("g").build(), Optional.empty(), 55),
            new Selection(7, leagueMemberBuilder().name("h").build(), Optional.empty(), 56),
            new Selection(8, leagueMemberBuilder().name("h").build(), Optional.empty(), 57),
            new Selection(8, leagueMemberBuilder().name("g").build(), Optional.empty(), 58),
            new Selection(8, leagueMemberBuilder().name("f").build(), Optional.empty(), 59),
            new Selection(8, leagueMemberBuilder().name("e").build(), Optional.empty(), 60),
            new Selection(8, leagueMemberBuilder().name("d").build(), Optional.empty(), 61),
            new Selection(8, leagueMemberBuilder().name("c").build(), Optional.empty(), 62),
            new Selection(8, leagueMemberBuilder().name("b").build(), Optional.empty(), 63),
            new Selection(8, leagueMemberBuilder().name("a").build(), Optional.empty(), 64)
        );

        assertThat(sut.arbitrate(rqst.getLeagueMembers(), rqst.getRounds()), equalTo(new DraftBoard(selections)));

        DraftBoard arbitrate = sut.arbitrate(rqst.getLeagueMembers(), rqst.getRounds());
        assertThat(arbitrate, equalTo(new DraftBoard(selections)));
    }

    @Test
    public void arbitrate_returnsDraftBoard_oneKeeper() {
        Keeper gronk = keeperBuilder().id(1).name("Gronk").round(3).penalty(5).build();
        LeagueMember member = leagueMemberBuilder()
            .name("a")
            .firstKeeper(gronk)
            .build();
        rqst = new ArbitrateDraftRequest(
            8,
            asList(
                member,
                leagueMemberBuilder().name("b").build(),
                leagueMemberBuilder().name("c").build(),
                leagueMemberBuilder().name("d").build(),
                leagueMemberBuilder().name("e").build(),
                leagueMemberBuilder().name("f").build(),
                leagueMemberBuilder().name("g").build(),
                leagueMemberBuilder().name("h").build()
            )
        );
        DraftBoard actual = sut.arbitrate(rqst.getLeagueMembers(), rqst.getRounds());

        List<Selection> keepee = actual.getSelections().stream()
            .filter(s -> s.getLeagueMember().getName().equals(member.getName()))
            .collect(toList());

        assertThat(keepee.size(), equalTo(8));
        assertThat(keepee.get(0).getRound(), equalTo(1));
        assertThat(keepee.get(1).getRound(), equalTo(2));

        Selection keeperRound = keepee.get(2);
        assertThat(keeperRound.getRound(), equalTo(3));
        assertThat(keeperRound.getPosition(), equalTo(17));
        assertThat(keeperRound.getPick().get(), equalTo("Gronk"));

        assertThat(keepee.get(3).getRound(), equalTo(4));
        assertThat(keepee.get(4).getRound(), equalTo(6));
        assertThat(keepee.get(5).getRound(), equalTo(6));
        assertThat(keepee.get(6).getRound(), equalTo(8));
        assertThat(keepee.get(7).getRound(), equalTo(8));
    }

    @Test
    public void arbitrate_returnsDraftBoardWithEqualAmountOfPlayersPerRound() {
        List<Selection> selections = sut.arbitrate(rqst.getLeagueMembers(), rqst.getRounds()).getSelections();

        List<Integer> rd1 = picks(selections, 1, 0, 8);
        List<Integer> rd2 = picks(selections, 2, 8, 16);
        List<Integer> rd3 = picks(selections, 3, 16, 24);
        List<Integer> rd4 = picks(selections, 4, 24, 32);
        List<Integer> rd5 = picks(selections, 5, 32, 40);
        List<Integer> rd6 = picks(selections, 6, 40, 48);
        List<Integer> rd7 = picks(selections, 7, 48, 56);
        List<Integer> rd8 = picks(selections, 8, 56, 64);


        assertThat(rd1.size(), equalTo(8));
        assertThat(rd2.size(), equalTo(8));
        assertThat(rd3.size(), equalTo(8));
        assertThat(rd4.size(), equalTo(8));
        assertThat(rd5.size(), equalTo(8));
        assertThat(rd6.size(), equalTo(8));
        assertThat(rd7.size(), equalTo(8));
        assertThat(rd8.size(), equalTo(8));
    }

    @Test
    public void arbitrate_returnsDraftBoardWithEqualAmountOfPlayersPerRound_oneKeeper() {
        Keeper gronk = keeperBuilder().id(1).name("Gronk").round(3).penalty(5).build();
        LeagueMember member = leagueMemberBuilder()
            .name("a")
            .firstKeeper(gronk)
            .build();
        rqst = new ArbitrateDraftRequest(
            8,
            asList(
                member,
                leagueMemberBuilder().name("b").build(),
                leagueMemberBuilder().name("c").build(),
                leagueMemberBuilder().name("d").build(),
                leagueMemberBuilder().name("e").build(),
                leagueMemberBuilder().name("f").build(),
                leagueMemberBuilder().name("g").build(),
                leagueMemberBuilder().name("h").build()
            )
        );

        List<Selection> selections = sut.arbitrate(rqst.getLeagueMembers(), rqst.getRounds()).getSelections();

        List<Integer> rd1 = picks(selections, 1, 0, 8);
        List<Integer> rd2 = picks(selections, 2, 8, 16);
        List<Integer> rd3 = picks(selections, 3, 16, 24);
        List<Integer> rd4 = picks(selections, 4, 24, 32);
        List<Integer> rd5 = picks(selections, 5, 32, 40);
        List<Integer> rd6 = picks(selections, 6, 40, 48);
        List<Integer> rd7 = picks(selections, 7, 48, 56);
        List<Integer> rd8 = picks(selections, 8, 56, 64);

        assertThat(rd1.size(), equalTo(8));
        assertThat(rd2.size(), equalTo(8));
        assertThat(rd3.size(), equalTo(8));
        assertThat(rd4.size(), equalTo(8));
        assertThat(rd5.size(), equalTo(8));
        assertThat(rd6.size(), equalTo(8));
        assertThat(rd7.size(), equalTo(8));
        assertThat(rd8.size(), equalTo(8));
    }

    private List<Integer> picks(List<Selection> selections, int round, int start, int finish) {
        return selections.subList(start, finish).stream()
            .map(Selection::getRound)
            .filter(s -> s.equals(round))
            .collect(toList());
    }
}