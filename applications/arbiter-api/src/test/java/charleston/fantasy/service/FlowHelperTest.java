package charleston.fantasy.service;

import charleston.fantasy.model.LeagueMember;
import charleston.fantasy.model.LeagueMemberBuilder;
import charleston.fantasy.model.Selection;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.util.Pair;

import java.util.List;
import java.util.stream.Collectors;

import static charleston.fantasy.model.KeeperBuilder.keeperBuilder;
import static charleston.fantasy.model.SelectionBuilder.selectionBuilder;
import static java.util.Arrays.asList;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class FlowHelperTest {

    private FlowHelper helper;
    private List<LeagueMember> members;

    @Before
    public void setup() throws Exception {
        helper = new FlowHelper(new KeeperHelper());

        members = asList(
            LeagueMemberBuilder.leagueMemberBuilder().id(1).name("a").build(),
            LeagueMemberBuilder.leagueMemberBuilder().id(2).name("b").build(),
            LeagueMemberBuilder.leagueMemberBuilder().id(3).name("c").build()
        );
    }

    @Test
    public void forward_returnsPair_withMembersListedInDraftOrder_andNewPosition() {
        Pair<List<Selection>, Integer> reverse = helper.forward(members, 5, 1);

        assertThat(reverse.getFirst().size(), equalTo(3));
        assertThat(reverse.getFirst(), equalTo(asList(
            selectionBuilder()
                .round(5)
                .position(1)
                .leagueMember(members.get(0))
                .build(),
            selectionBuilder()
                .round(5)
                .position(2)
                .leagueMember(members.get(1))
                .build(),
            selectionBuilder()
                .round(5)
                .position(3)
                .leagueMember(members.get(2))
                .build()
        )));
        assertThat(reverse.getSecond(), equalTo(4));
    }

    @Test
    public void forward_returnsPair_withMembersListedInDraftOrder_whereOneMemberIsExcluded_andNewPosition() {
        List<LeagueMember> members = asList(
            LeagueMemberBuilder.leagueMemberBuilder()
                .id(1)
                .name("a")
                .firstKeeper(keeperBuilder().penalty(5).build())
                .build(),
            LeagueMemberBuilder.leagueMemberBuilder().id(2).name("b").build(),
            LeagueMemberBuilder.leagueMemberBuilder().id(3).name("c").build()
        );
        Pair<List<Selection>, Integer> reverse = helper.forward(members, 5, 1);

        assertThat(reverse.getFirst().size(), equalTo(2));
        assertThat(reverse.getFirst(), equalTo(asList(
            selectionBuilder()
                .round(5)
                .position(1)
                .leagueMember(members.get(1))
                .build(),
            selectionBuilder()
                .round(5)
                .position(2)
                .leagueMember(members.get(2))
                .build()
        )));
        assertThat(reverse.getSecond(), equalTo(3));
    }

    @Test
    public void reverse_returnsPair_withMembersListedInReverseOrder_andNewPosition() {
        Pair<List<Selection>, Integer> reverse = helper.reverse(members, 10, 1);

        assertThat(reverse.getFirst().size(), equalTo(3));
        assertThat(reverse.getFirst(), equalTo(asList(
            selectionBuilder()
                .round(10)
                .position(1)
                .leagueMember(members.get(2))
                .build(),
            selectionBuilder()
                .round(10)
                .position(2)
                .leagueMember(members.get(1))
                .build(),
            selectionBuilder()
                .round(10)
                .position(3)
                .leagueMember(members.get(0))
                .build()
        )));
        assertThat(reverse.getSecond(), equalTo(4));
    }

    @Test
    public void reverse_returnsPair_withMembersListedInDraftOrder_whereOneMemberIsExcluded_andNewPosition() {
        List<LeagueMember> members = asList(
            LeagueMemberBuilder.leagueMemberBuilder()
                .id(1)
                .name("a")
                .firstKeeper(keeperBuilder().penalty(5).build())
                .build(),
            LeagueMemberBuilder.leagueMemberBuilder().id(2).name("b").build(),
            LeagueMemberBuilder.leagueMemberBuilder().id(3).name("c").build()
        );
        Pair<List<Selection>, Integer> reverse = helper.reverse(members, 5, 1);

        assertThat(reverse.getFirst().size(), equalTo(2));
        assertThat(reverse.getFirst(), equalTo(asList(
            selectionBuilder()
                .round(5)
                .position(1)
                .leagueMember(members.get(2))
                .build(),
            selectionBuilder()
                .round(5)
                .position(2)
                .leagueMember(members.get(1))
                .build()
        )));
        assertThat(reverse.getSecond(), equalTo(3));
    }

    @Test
    public void goForward_returnsStreamOfPlayersInDraftOrder() {
        List<LeagueMember> actual = helper.goForward(members).collect(Collectors.toList());
        assertThat(actual, equalTo(members));
    }

    @Test
    public void goBackward_returnsStreamOfPlayersInReverseOrder() {
        List<LeagueMember> actual = helper.goBackward(members).collect(Collectors.toList());
        assertThat(actual, equalTo(asList(members.get(2), members.get(1), members.get(0))));
    }
}