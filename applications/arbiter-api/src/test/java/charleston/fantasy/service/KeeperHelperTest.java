package charleston.fantasy.service;

import charleston.fantasy.model.LeagueMember;
import org.junit.Before;
import org.junit.Test;

import static charleston.fantasy.model.KeeperBuilder.keeperBuilder;
import static charleston.fantasy.model.LeagueMemberBuilder.leagueMemberBuilder;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class KeeperHelperTest {
    private KeeperHelper helper;

    @Before
    public void setup() throws Exception {
        helper = new KeeperHelper();
    }

    @Test
    public void excluded_returnsTrue_whenFirstKeeperPenaltyEqualsRound_andSecondKeeperIsNotPresent() {
        LeagueMember member = leagueMemberBuilder()
            .firstKeeper(keeperBuilder().penalty(10).build())
            .build();

        assertThat(helper.excluded(member, 10), equalTo(true));
    }

    @Test
    public void excluded_returnsTrue_whenFirstKeeperIsNotPresent_andSecondKeeperPenaltyEqualsRound() {
        LeagueMember member = leagueMemberBuilder()
            .secondKeeper(keeperBuilder().penalty(10).build())
            .build();

        assertThat(helper.excluded(member, 10), equalTo(true));
    }

    @Test
    public void excluded_returnsTrue_whenFirstKeeperPenaltyEqualsRound_andSecondKeeperPenaltyEqualsRound() {
        LeagueMember member = leagueMemberBuilder()
            .firstKeeper(keeperBuilder().penalty(10).build())
            .secondKeeper(keeperBuilder().penalty(10).build())
            .build();

        assertThat(helper.excluded(member, 10), equalTo(true));
    }

    @Test
    public void excluded_returnsTrue_whenFirstKeeperIsPresent_andSecondKeeperPenaltyEqualsRound() {
        LeagueMember member = leagueMemberBuilder()
            .firstKeeper(keeperBuilder().penalty(5).build())
            .secondKeeper(keeperBuilder().penalty(11).build())
            .build();

        assertThat(helper.excluded(member, 11), equalTo(true));
    }

    @Test
    public void excluded_returnsFalse_whenKeepersAreNotPresent() {
        LeagueMember member = leagueMemberBuilder().build();

        assertThat(helper.excluded(member, 1), equalTo(false));
    }
}