package charleston.fantasy.service;


import charleston.fantasy.model.LeagueMember;
import charleston.fantasy.model.Selection;
import org.jooq.lambda.Seq;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
class FlowHelper {
    private KeeperHelper helper;
    private Integer position;

    public FlowHelper(KeeperHelper helper) {
        this.helper = helper;
    }

    Pair<List<Selection>, Integer> forward(List<LeagueMember> members, Integer round, Integer position) {
        this.position = position;

        return Pair.of(goForward(members)
            .filter(leagueMember -> !helper.excluded(leagueMember, round))
            .map(leagueMember -> helper.getSelection(leagueMember, round, this.position++))
            .collect(toList()), this.position);
    }

    Pair<List<Selection>, Integer> reverse(List<LeagueMember> members, Integer round, Integer position) {
        this.position = position;

        return Pair.of(goBackward(members)
            .filter(leagueMember -> !helper.excluded(leagueMember, round))
            .map(leagueMember -> helper.getSelection(leagueMember, round, this.position++))
            .collect(toList()), this.position);
    }

    Stream<LeagueMember> goForward(List<LeagueMember> members) {
        return Seq.seq(members).stream();
    }

    Stream<LeagueMember> goBackward(List<LeagueMember> members) {
        return Seq.seq(members).reverse().stream();
    }
}