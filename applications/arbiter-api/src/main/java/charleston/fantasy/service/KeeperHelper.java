package charleston.fantasy.service;

import charleston.fantasy.model.Keeper;
import charleston.fantasy.model.LeagueMember;
import charleston.fantasy.model.Selection;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static java.util.Arrays.asList;

@Component
class KeeperHelper {
    boolean excluded(LeagueMember member, Integer round) {
        Optional<Keeper> firstKeeper = member.getFirstKeeper();
        Optional<Keeper> secondKeeper = member.getSecondKeeper();

        if (firstKeeper.isPresent()) {
            if (secondKeeper.isPresent()) {
                return asList(firstKeeper.get().getPenalty(), secondKeeper.get().getPenalty())
                    .contains(round);
            } else {
                return firstKeeper.get().getPenalty().equals(round);
            }
        } else return secondKeeper.map(keeper -> keeper.getPenalty().equals(round))
            .orElse(false);
    }

    Selection getSelection(LeagueMember member, Integer round, Integer position) {
        Optional<Keeper> firstKeeper = member.getFirstKeeper();
        Optional<Keeper> secondKeeper = member.getSecondKeeper();

        if (firstKeeper.isPresent() && firstKeeper.get().getRound().equals(round)) {
            return new Selection(
                round,
                member,
                Optional.of(firstKeeper.get().getName()),
                position);
        } else if (secondKeeper.isPresent() && secondKeeper.get().getRound().equals(round)) {
            return new Selection(
                round,
                member,
                Optional.of(secondKeeper.get().getName()),
                position);
        }

        return new Selection(round, member, Optional.empty(), position);
    }
}