package charleston.fantasy.service;

import charleston.fantasy.model.DraftBoard;
import charleston.fantasy.model.LeagueMember;
import charleston.fantasy.model.Selection;
import org.apache.commons.collections4.ListUtils;
import org.jooq.lambda.Seq;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static charleston.fantasy.model.SelectionBuilder.selectionBuilder;
import static java.util.stream.Collectors.toList;

@Component
public class DraftBoardService {
    private Integer position;
    private FlowHelper flow;
    private KeeperHelper keeper;

    public DraftBoardService(FlowHelper flow, KeeperHelper keeper) {
        this.flow = flow;
        this.keeper = keeper;
    }

    public DraftBoard arbitrate(List<LeagueMember> members, Integer rounds) {
        this.position = 1;

        List<Selection> selections = ListUtils.union(
            getDraftBoard(members, rounds), getCompensatoryPicks(members, rounds));
        return new DraftBoard(rebuildRounds(members, rounds, selections));
    }

    private List<Selection> getDraftBoard(List<LeagueMember> members, Integer rounds) {
        return Seq.rangeClosed(1, rounds)
            .map(round -> {
                if (round % 2 == 0) {
                    Pair<List<Selection>, Integer> results = flow.reverse(members, round, position);
                    this.position = results.getSecond();
                    return results.getFirst();
                } else {
                    Pair<List<Selection>, Integer> results = flow.forward(members, round, position);
                    this.position = results.getSecond();
                    return results.getFirst();
                }
            })
            .flatMap(Collection::stream)
            .collect(toList());
    }

    private List<Selection> getCompensatoryPicks(List<LeagueMember> members, Integer rounds) {
        return Seq.rangeClosed(1, rounds)
            .map(round -> {
                if (round % 2 == 0) {
                    return getCompensatoryPick(flow.goBackward(members), round);
                } else {
                    return getCompensatoryPick(flow.goForward(members), round);
                }
            })
            .flatMap(Collection::stream)
            .collect(toList());
    }

    private List<Selection> getCompensatoryPick(Stream<LeagueMember> leagueMembers, Integer round) {
        return leagueMembers
            .filter(leagueMember -> keeper.excluded(leagueMember, round))
            .map(leagueMember -> new Selection(999, leagueMember, Optional.empty(), position++))
            .collect(toList());
    }

    private List<Selection> rebuildRounds(List<LeagueMember> members, Integer rounds, List<Selection> selections) {
        int mltplr = members.size();

        return Seq.rangeClosed(1, rounds)
            .map(round -> {
                int start = (mltplr * round) - mltplr;
                int stop = mltplr * round;

                return selections.subList(start, stop).stream()
                    .map(s -> selectionBuilder()
                        .leagueMember(s.getLeagueMember())
                        .position(s.getPosition())
                        .round(round)
                        .pick(s.getPick().orElse(null))
                        .build()
                    )
                    .collect(toList());
            })
            .flatMap(Collection::stream)
            .collect(toList());
    }
}