package charleston.fantasy.controller;

import charleston.fantasy.model.ArbitrateDraftRequest;
import charleston.fantasy.model.DraftBoard;
import charleston.fantasy.service.DraftBoardService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api")
public class DraftController {

    private DraftBoardService draftBoardService;

    public DraftController(DraftBoardService draftBoardService) {
        this.draftBoardService = draftBoardService;
    }

    @ResponseBody
    @RequestMapping(
        value = "/draft/arbitrate",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    DraftBoard draft(@RequestBody ArbitrateDraftRequest request) {
        return draftBoardService.arbitrate(request.getLeagueMembers(), request.getRounds());

        //debug
//        final String NO_KEEPER = "Empty Slot";
//        generated.getSelections().forEach(selection -> {
//            String pick = selection.getPick().isPresent() ? selection.getPick().get() : NO_KEEPER;
//            System.out.printf("%03d %03d %-20s %-20s \n",
//                selection.getRound(),
//                selection.getPosition(),
//                selection.getLeagueMember().getName(),
//                pick
//            );
//        });
//        return generated;
    }
}
