package charleston.fantasy.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Optional;

public class LeagueMember {
    private final Integer id;
    private final String name;
    private final Optional<Keeper> firstKeeper;
    private final Optional<Keeper> secondKeeper;

    @JsonCreator
    public LeagueMember(
        @JsonProperty("id") Integer id,
        @JsonProperty("name") String name,
        @JsonProperty("firstKeeper") Optional<Keeper> firstKeeper,
        @JsonProperty("secondKeeper") Optional<Keeper> secondKeeper
    ) {
        this.id = id;
        this.name = name;
        this.firstKeeper = firstKeeper;
        this.secondKeeper = secondKeeper;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Optional<Keeper> getFirstKeeper() {
        return firstKeeper;
    }

    public Optional<Keeper> getSecondKeeper() {
        return secondKeeper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LeagueMember that = (LeagueMember) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (firstKeeper != null ? !firstKeeper.equals(that.firstKeeper) : that.firstKeeper != null) return false;
        return secondKeeper != null ? secondKeeper.equals(that.secondKeeper) : that.secondKeeper == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (firstKeeper != null ? firstKeeper.hashCode() : 0);
        result = 31 * result + (secondKeeper != null ? secondKeeper.hashCode() : 0);
        return result;
    }
}