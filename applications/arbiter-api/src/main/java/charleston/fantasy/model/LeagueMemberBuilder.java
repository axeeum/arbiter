package charleston.fantasy.model;

import java.util.Optional;

public class LeagueMemberBuilder {
    private Integer id;
    private String name;
    private Keeper firstKeeper;
    private Keeper secondKeeper;

    private LeagueMemberBuilder() { }

    public static LeagueMemberBuilder leagueMemberBuilder() {
        return new LeagueMemberBuilder();
    }

    public LeagueMemberBuilder id(Integer id) {
        this.id = id;
        return this;
    }

    public LeagueMemberBuilder name(String name) {
        this.name = name;
        return this;
    }

    public LeagueMemberBuilder firstKeeper(Keeper firstKeeper) {
        this.firstKeeper = firstKeeper;
        return this;
    }

    public LeagueMemberBuilder secondKeeper(Keeper secondKeeper) {
        this.secondKeeper = secondKeeper;
        return this;
    }

    public LeagueMember build() {
        return new LeagueMember(
            id, name,
            Optional.ofNullable(firstKeeper),
            Optional.ofNullable(secondKeeper)
        );
    }
}
