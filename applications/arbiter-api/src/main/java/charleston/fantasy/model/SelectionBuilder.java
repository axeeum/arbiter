package charleston.fantasy.model;

import java.util.Optional;

public class SelectionBuilder {
    private Integer round;
    private LeagueMember leagueMember;
    private String pick;
    private Integer position;

    private SelectionBuilder() { }

    public static SelectionBuilder selectionBuilder() {
        return new SelectionBuilder();
    }

    public SelectionBuilder round(Integer round) {
        this.round = round;
        return this;
    }

    public SelectionBuilder leagueMember(LeagueMember leagueMember) {
        this.leagueMember = leagueMember;
        return this;
    }

    public SelectionBuilder pick(String pick) {
        this.pick = pick;
        return this;
    }

    public SelectionBuilder position(Integer position) {
        this.position = position;
        return this;
    }

    public Selection build() {
        return new Selection(round, leagueMember, Optional.ofNullable(pick), position);
    }
}
