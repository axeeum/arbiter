package charleston.fantasy.model;


import java.util.Optional;

public class Selection {
    private final Integer round;
    private final LeagueMember leagueMember;
    private final Optional<String> pick;
    private final Integer position;

    public Selection(Integer round, LeagueMember leagueMember, Optional<String> pick, Integer position) {
        this.round = round;
        this.leagueMember = leagueMember;
        this.pick = pick;
        this.position = position;
    }

    public Integer getRound() {
        return round;
    }

    public LeagueMember getLeagueMember() {
        return leagueMember;
    }

    public Optional<String> getPick() {
        return pick;
    }

    public Integer getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Selection selection = (Selection) o;

        if (round != null ? !round.equals(selection.round) : selection.round != null) return false;
        if (leagueMember != null ? !leagueMember.equals(selection.leagueMember) : selection.leagueMember != null)
            return false;
        if (pick != null ? !pick.equals(selection.pick) : selection.pick != null) return false;
        return position != null ? position.equals(selection.position) : selection.position == null;

    }

    @Override
    public int hashCode() {
        int result = round != null ? round.hashCode() : 0;
        result = 31 * result + (leagueMember != null ? leagueMember.hashCode() : 0);
        result = 31 * result + (pick != null ? pick.hashCode() : 0);
        result = 31 * result + (position != null ? position.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Selection{" +
            "round=" + round +
            ", leagueMember=" + leagueMember +
            ", pick=" + pick +
            ", position=" + position +
            '}';
    }
}
