package charleston.fantasy.model;

import java.util.List;

public class DraftBoard {
    private final List<Selection> selections;

    public DraftBoard(List<Selection> selections) {
        this.selections = selections;
    }

    public List<Selection> getSelections() {
        return selections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DraftBoard that = (DraftBoard) o;
        return selections != null ? selections.equals(that.selections) : that.selections == null;
    }

    @Override
    public int hashCode() {
        return 31 * (selections != null ? selections.hashCode() : 0);
    }

    @Override
    public String toString() {
        return "DraftBoard{" +
            ", selections=" + selections +
            '}';
    }
}
