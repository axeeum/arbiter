package charleston.fantasy.model;

public class KeeperBuilder {
    private Integer id;
    private String name;
    private int round;
    private int penalty;

    private KeeperBuilder() { }

    public static KeeperBuilder keeperBuilder() {
        return new KeeperBuilder();
    }

    public KeeperBuilder id(Integer id) {
        this.id = id;
        return this;
    }

    public KeeperBuilder name(String name) {
        this.name = name;
        return this;
    }

    public KeeperBuilder round(int round) {
        this.round = round;
        return this;
    }

    public KeeperBuilder penalty(int penalty) {
        this.penalty = penalty;
        return this;
    }

    public Keeper build() {
        return new Keeper(id, name, round, penalty);
    }
}