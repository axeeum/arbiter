package charleston.fantasy.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ArbitrateDraftRequest {
    private Integer rounds;
    private List<LeagueMember> leagueMembers;

    @JsonCreator
    public ArbitrateDraftRequest(
        @JsonProperty("rounds") Integer rounds,
        @JsonProperty("leagueMembers") List<LeagueMember> leagueMembers
    ) {
        this.rounds = rounds;
        this.leagueMembers = leagueMembers;
    }

    public Integer getRounds() {
        return rounds;
    }

    public List<LeagueMember> getLeagueMembers() {
        return leagueMembers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArbitrateDraftRequest that = (ArbitrateDraftRequest) o;

        if (rounds != null ? !rounds.equals(that.rounds) : that.rounds != null) return false;
        return leagueMembers != null ? leagueMembers.equals(that.leagueMembers) : that.leagueMembers == null;
    }

    @Override
    public int hashCode() {
        int result = rounds != null ? rounds.hashCode() : 0;
        result = 31 * result + (leagueMembers != null ? leagueMembers.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ArbitrateDraftRequest{" +
            "rounds=" + rounds +
            ", leagueMembers=" + leagueMembers +
            '}';
    }
}
