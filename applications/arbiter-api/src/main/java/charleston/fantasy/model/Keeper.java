package charleston.fantasy.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Keeper {
    private final Integer id;
    private final String name;
    private final Integer round;
    private final Integer penalty;

    @JsonCreator
    public Keeper(
        @JsonProperty("id")  Integer id,
        @JsonProperty("name") String name,
        @JsonProperty("round") Integer round,
        @JsonProperty("penalty") Integer penalty
    ) {
        this.id = id;
        this.name = name;
        this.round = round;
        this.penalty = penalty;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getRound() {
        return round;
    }

    public Integer getPenalty() {
        return penalty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Keeper keeper = (Keeper) o;

        if (round != keeper.round) return false;
        if (penalty != keeper.penalty) return false;
        if (id != null ? !id.equals(keeper.id) : keeper.id != null) return false;
        return name != null ? name.equals(keeper.name) : keeper.name == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + round;
        result = 31 * result + penalty;
        return result;
    }

    @Override
    public String toString() {
        return "Keeper{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", round=" + round +
            ", penalty=" + penalty +
            '}';
    }
}