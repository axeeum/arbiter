package charleston.fantasy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"charleston.fantasy"})
public class ArbiterApi {

	public static void main(String[] args) {
        SpringApplication.run(ArbiterApi.class, args);
	}
}
