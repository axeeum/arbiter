let webpack = require('webpack');
let path = require('path');
let root = path.resolve(__dirname, './src/main');
let frontend = path.resolve(root, 'frontend');
let output_location = path.resolve(root, 'resources/static/public')

let HtmlWebpackPlugin = require('html-webpack-plugin')
let ExtractTextPlugin = require('extract-text-webpack-plugin')
let CleanWebpackPlugin = require('clean-webpack-plugin');

const prod = process.argv.indexOf('-p') !== -1;

module.exports = {
    devtool: 'cheap-module-source-map',
    entry: {
        app: path.resolve(frontend, 'main/index.jsx')
    },
    resolve: {
        root: [path.resolve(root, 'frontend')],
        extensions: ['', '.js', '.jsx']
    },
    output: {
        path: output_location,
        filename: '[name]-[hash].js',
        sourceMapFilename: '[name].[hash].js.map',
        chunkFilename: '[id].chunk.js',
        publicPath: '/public/'
    },
    plugins: [
        new CleanWebpackPlugin([output_location], {root: root}),
        new HtmlWebpackPlugin({
            filename: path.resolve(root, 'resources/templates/index.html'),
            template: path.resolve(frontend, 'main/index.html'),
            inject: 'body',
            xhtml: true
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
        }),
        new ExtractTextPlugin("[name]-[hash].css"),

        new webpack.DefinePlugin({ 'process.env': { 'NODE_ENV': prod? `"production"`: `"development"` }}),

        // new webpack.optimize.UglifyJsPlugin({ compressor: { warnings: false }}),
        // new webpack.optimize.OccurenceOrderPlugin(),
        // new webpack.optimize.DedupePlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react', 'stage-0', 'stage-3']
                }
            },
            {
                test: /\.scss$/,
                exclude: [/node_modules/],
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")
            },
            {
                test: /react-icons\/(.)*(.js)$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            },
        ]
    },
    sassLoader: {
        includePaths: [path.resolve(__dirname, "node_modules")]
    }
};
