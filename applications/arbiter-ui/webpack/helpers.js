
module.exports = {
  parseDotenvConfig: function(config) {
    const define = {};
    for (let key in config) {
      if (config.hasOwnProperty(key)) {
        define[key] = JSON.stringify(config[key]);
      }
    }
    return define;
  }
};
