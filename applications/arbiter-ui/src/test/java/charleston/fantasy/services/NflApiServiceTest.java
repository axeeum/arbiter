package charleston.fantasy.services;

import charleston.fantasy.models.Nfl;
import charleston.fantasy.models.Player;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class NflApiServiceTest {

    private NflApiService nflApiService;

    @Before
    public void setUp() throws URISyntaxException {
        Path path = Paths.get(getClass().getResource("/players-subset.json").toURI());
        nflApiService = new NflApiService(path, new ObjectMapper());
    }

    @Test
    public void getNfl_mapsJsonToModel() throws URISyntaxException, IOException {
        Nfl expected = new Nfl(asList(
            new Player(1L, "aa", "aa","wr", "ne",1L),
            new Player(2L, "ab", "ab","rb", "nyg",2L),
            new Player(3L, "ac", "ac","te", "gb",3L)
        ));

        assertThat(nflApiService.getTop100(), equalTo(expected));
    }
}