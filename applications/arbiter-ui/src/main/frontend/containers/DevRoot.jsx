import React, {Component} from 'react'
import {Provider} from 'react-redux'
import {Router, browserHistory} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import {buildStore} from '../helpers/store'
import routes from '../routes'

const store = buildStore()
const history = syncHistoryWithStore(browserHistory, store)

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router onUpdate={() => window.scrollTo(0, 0)} history={history} routes={routes} />
            </Provider>
        )
    }
}