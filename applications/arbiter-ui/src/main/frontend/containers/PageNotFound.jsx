import { Component, PropTypes } from 'react'

export default class PageNotFound extends Component {

  render() {
    return (
      <div>
        <h3>Page not found</h3>
        <div>The page "<strong>{this.props.url}</strong>" does not exist.</div>
      </div>
    )
  }
}

PageNotFound.propTypes = {
  url: PropTypes.string.isRequired
}
