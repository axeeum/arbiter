if (process.env.NODE_ENV === 'production') {
    module.exports = require('./ProdRoot.jsx')
} else {
    module.exports = require('./DevRoot.jsx')
}