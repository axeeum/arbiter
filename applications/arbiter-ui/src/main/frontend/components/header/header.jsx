import React, {PropTypes} from 'react'

const Header = ({title}) => {
    return (
        <div className="hero">
            <div className="row">
                <div className="large-12 columns">
                    <h1>{title}</h1>
                </div>
            </div>
        </div>
    )
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
}

export default Header