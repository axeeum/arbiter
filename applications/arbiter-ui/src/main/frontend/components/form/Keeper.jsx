import React, {PropTypes} from 'react'
import * as _ from 'lodash'
import {connect} from 'react-redux'
import Select from 'react-select'
import {selectKeeper, removeKeeper} from '../../state/app/actions'
import * as keeperHelper from '../../helpers/keeper'

export const Keeper = ({rounds, member, identifier, players, select, remove}) => {

    const parts = _.kebabCase(identifier).split('-')
    const keeper = member[identifier]

    return (
        <div className="keeper-container">
            <label>{[_.capitalize(parts[0]), _.capitalize(parts[1])].join(" ")}
                <Select
                    simpleValue
                    options={keeperHelper.options(players)}
                    value={!keeperHelper.isNull(keeper) ? keeper.id : null}
                    onChange={keeperHelper.handleKeeper(rounds, member, players, identifier, select, remove)}/>
            </label>
            <label>Drafted
                <select value={!keeperHelper.isNull(keeper) ? keeper.round : 0}
                        onChange={(e) => {
                            keeperHelper.handleExclusions(rounds, member, identifier, e.target.value, select)
                        }}>

                    <option key={0} value="0">N/A</option>

                    {[...Array(rounds).keys()].map((key) => {
                        const round = key + 1
                        return <option key={round} value={round}>{round}</option>
                    })}
                </select>
            </label>
        </div>
    )
}

Keeper.displayName = 'Keeper'
Keeper.propTypes = {
    select: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired,
    players: PropTypes.array.isRequired,
    member: PropTypes.object.isRequired,
    identifier: PropTypes.string.isRequired,
}

export const mapStateToProps = (state) => {
    return {
        rounds: state.app.rounds,
        players: state.app.nfl.players
    }
}

export const mapDispatchToProps = (dispatch) => {
    return {
        select: (id, keeper) => {
            dispatch(selectKeeper(id, keeper))
        },
        remove: (id, identifier) => {
            dispatch(removeKeeper(id, identifier))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Keeper)