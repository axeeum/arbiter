import React, {PropTypes} from 'react'
import Keeper from './Keeper'
import PlayerName from './PlayerName'
import {connect} from 'react-redux'

export const LeagueMember = ({member, players}) => {
    return (
        <div className="league-member row">
            <div className="medium-3 columns">
                <PlayerName member={member} />
            </div>
            <div className="medium-1 columns"/>
            <div className="medium-4 columns">
                <Keeper
                    identifier={"firstKeeper"}
                    member={member}
                    players={players}/>
            </div>
            <div className="medium-4 columns">
                <Keeper
                    identifier={"secondKeeper"}
                    member={member}
                    players={players}/>
            </div>
        </div>
    )
}

LeagueMember.displayName = 'LeagueMember'
LeagueMember.propTypes = {
    member: PropTypes.object.isRequired,
    players: PropTypes.array.isRequired,
}

export const mapStateToProps = (state) => {
    return {
        players: state.app.nfl.players
    }
}

export default connect(mapStateToProps, {})(LeagueMember)