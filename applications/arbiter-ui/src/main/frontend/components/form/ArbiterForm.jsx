import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Header from '../header/Header'
import RoundPicker from './RoundPicker'
import ArbiterFormControls from './ArbiterFormControls'
import LeagueMember from './LeagueMember'
import {fetchPlayers} from '../../state/app/actions'

export class ArbiterForm extends React.Component {
    componentWillMount() {
        this.props.fetchPlayers()
    }

    render() {
        const {leagueMembers} = this.props

        return (
            <div>
                <Header title="Arbiter"/>

                <RoundPicker />

                {leagueMembers.map((m, idx) => {
                    return <LeagueMember key={idx} member={m}/>
                })}

                <ArbiterFormControls />
            </div>
        )
    }
}

ArbiterForm.displayName = 'ArbiterForm'
ArbiterForm.propTypes = {
    leagueMembers: PropTypes.array.isRequired,
}

export const mapStateToProps = (state) => {
    return {
        leagueMembers: state.app.leagueMembers,
    }
}

export const mapDispatchToProps = (dispatch) => {
    return {
        fetchPlayers() {
            dispatch(fetchPlayers())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArbiterForm)