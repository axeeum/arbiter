import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Up from 'react-icons/go/arrow-up'
import Down from 'react-icons/go/arrow-down'
import {increaseRounds, decreaseRounds} from '../../state/app/actions'

export const RoundPicker = ({rounds, increase, decrease}) => {
    return (
        <div className='row roundPicker'>
            <div className='medium-4 columns'/>
            <div className='medium-4 columns'>
                <label>Rounds</label>

                <div className="picker">
                    <Down size={40} className='decrease' onClick={() => {decrease()}}/>
                    <span className='rounds'>{rounds}</span>
                    <Up size={40} className='increase' onClick={() => {increase()}}/>
                </div>
            </div>
            <div className='medium-4 columns'/>
        </div>
    )
}

RoundPicker.displayName = 'RoundPicker'
RoundPicker.propTypes = {
    rounds: PropTypes.number.isRequired,
    increase: PropTypes.func.isRequired,
    decrease: PropTypes.func.isRequired,
}

export const mapStateToProps = (state) => {
    return {
        rounds: state.app.rounds
    }
}

export const mapDispatchToProps = (dispatch) => {
    return {
        increase: () => {
            dispatch(increaseRounds())
        },
        decrease: () => {
            dispatch(decreaseRounds())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoundPicker)