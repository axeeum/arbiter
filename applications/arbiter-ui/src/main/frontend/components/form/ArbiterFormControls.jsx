import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'
import {addLeagueMember} from '../../state/app/actions'

export const ArbiterFormControls = ({add}) => {
    return (
        <div className="controls row">
            <div className="medium-6 text-right columns">
                <Link to="/draftboard">
                    <button type="button" className="button large"> Arbitrate</button>
                </Link>
            </div>
            <div className="medium-6 columns">
                <button
                    type="button"
                    className="success button large"
                    onClick={() => {
                        add()
                    }}> Add Players
                </button>
            </div>
        </div>
    )
}

ArbiterFormControls.displayName = 'ArbiterFormControls'
ArbiterFormControls.propTypes = {
    add: PropTypes.func.isRequired,
}

export const mapDispatchToProps = (dispatch) => {
    return {
        add: () => {
            dispatch(addLeagueMember())
        }
    }
}

export default connect(_ => _, mapDispatchToProps)(ArbiterFormControls)