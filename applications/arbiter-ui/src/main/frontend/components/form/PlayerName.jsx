import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {updatePlayerName} from '../../state/app/actions'

export const PlayerName = ({member, update}) => {
    return (
        <label>Player
            <input
                type='text'
                onChange={(e) => {
                    update(member.id, e.target.value)
                }}
                defaultValue={member.name}
                placeholder='Player Name'/>
        </label>
    )
}

PlayerName.displayName = 'PlayerName'
PlayerName.propTypes = {
    member: PropTypes.object.isRequired,
    update: PropTypes.func.isRequired
}

export const mapDispatchToProps = (dispatch) => {
    return {
        update: (id, name) => {
            dispatch(updatePlayerName(id, name))
        }
    }
}

export default connect(_ => _, mapDispatchToProps)(PlayerName)