import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import Header from '../header/Header'
import Round from './Round'
import Selection from './Selection'
import * as keeper from '../../helpers/keeper'
import {arbitrateDraft} from '../../state/app/actions'

export class Draftboard extends React.Component {
    componentWillMount() {
        const {rounds, leagueMembers, arbitrate} = this.props

        const members = leagueMembers.map((member) => {
            return {
                ...member,
                firstKeeper: keeper.setRoundForUndrafted(rounds, member.firstKeeper, 1),
                secondKeeper: keeper.setRoundForUndrafted(rounds, member.secondKeeper, 2),
            }
        })

        arbitrate({rounds: rounds, leagueMembers: members})
    }

    render() {
        const {selections} = this.props

        const rounds = [
            ...new Set(selections.map((selection) => {
                return selection.round
            }))
        ]

        const draftboard = rounds.map((round) => {
            const picks = selections.map((selection) => {
                if (selection.round === round) {
                    return <Selection key={round + selection.position} round={round} selection={selection}/>
                }
            })

            return <Round key={round} round={round} selections={picks}/>
        })

        return (
            <div>
                <Header title="Arbiter"/>
                {draftboard}
            </div>
        )
    }
}

Draftboard.displayName = 'Draftboard'
Draftboard.propTypes = {
    rounds: PropTypes.number.isRequired,
    leagueMembers: PropTypes.array.isRequired,
    selections: PropTypes.array.isRequired,
    arbitrate: PropTypes.func.isRequired,
}

export const mapStateToProps = (state) => {
    return {
        rounds: state.app.rounds,
        leagueMembers: state.app.leagueMembers,
        selections: state.app.draftboard.selections,
    }
}

export const mapDispatchToProps = (dispatch) => {
    return {
        arbitrate: (league) => {
            dispatch(arbitrateDraft(league))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Draftboard)