import React, {PropTypes} from 'react'

const Round = ({round, selections}) => {
    return (
        <div className='row'>
            <div className='medium-2 columns'>
                <h1>Round {round}</h1>
            </div>
            <div className='medium-10 columns picks'>
                {selections}
            </div>
        </div>
    )
}

Round.displayName = 'Round'
Round.propTypes = {
    round: PropTypes.number.isRequired,
    selections: PropTypes.array.isRequired,
}

export default Round