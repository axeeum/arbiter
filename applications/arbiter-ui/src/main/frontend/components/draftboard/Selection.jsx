import React, {PropTypes} from 'react'

const Selection = ({round, selection}) => {
    const classes = 'selection callout ' + (round % 2 ? 'primary' : 'secondary')

    const name = selection.leagueMember.name
    const pick = selection.pick
    const position = selection.position

    return (
        <div className={classes}>
            <h5> { name ? name : '\<No Name Entered\>' }</h5>
            <p>Pick: {pick ? pick : '\u2014'.repeat(10)}</p>
            <p>Position: {position}</p>
        </div>
    )
}

Selection.displayName = 'Selection'
Selection.propTypes = {
    round: PropTypes.number.isRequired,
    selection: PropTypes.object.isRequired,
}

export default Selection