import React from 'react'
import {shallow} from 'enzyme'
import configureStore from 'redux-mock-store'
import Connected, {Keeper, mapDispatchToProps} from '../../../components/form/Keeper'
import ReactTestUtils from 'react-dom/lib/ReactTestUtils'
import Select from 'react-select'
import {selectKeeper, removeKeeper} from '../../../state/app/actions'

describe('Keeper', () => {
    const setup = () => {
        const props = {
            rounds: 10,
            member: {id: 1, keeper: {id: 1, name: 'xx xx', round: 0, penalty: null}},
            players: [
                {id: 1, firstName: 'xx', lastName: 'xx', position: 'k'},
                {id: 2, firstName: 'yy', lastName: 'yy', position: 'p'}
            ],
            identifier: 'keeper',
            select: jest.fn(),
            remove: jest.fn()
        }

        return {props, wrapper: shallow(<Keeper {...props} />)}
    }

    describe('rendering', () => {
        it('connected component matches snapshot', () => {
            const {props} = setup()
            const mocked = configureStore()({
                app: {
                    rounds: 10,
                    nfl: {
                        players: props.players,
                        select: props.select
                    }
                }
            })

            const shallowRenderer = ReactTestUtils.createRenderer()
            const rendered = shallowRenderer.render(
                <Connected
                    store={mocked}
                    member={props.member}
                    identifier={props.identifier}
                />)

            expect(rendered).toMatchSnapshot()
        })

        it('component matches snapshot', () => {
            const {props} = setup()

            const rendered = ReactTestUtils.createRenderer()
                .render(<Keeper {...props} />)

            expect(rendered).toMatchSnapshot()
        })
    })

    describe('property delegation', () => {
        it('maps dispatch to props', () => {
            const dispatch = jest.fn()
            const {select, remove} = mapDispatchToProps(dispatch)

            const keeper = {keeper: {id: 2, name: 'aa bb', round: 1, penalty: 3}}

            select(1, keeper)
            expect(dispatch).lastCalledWith(selectKeeper(1, keeper))

            remove(1, 'identifier')
            expect(dispatch).lastCalledWith(removeKeeper(1, 'identifier'))
        })

        it('builds options from list of players', () => {
            const {props, wrapper} = setup()

            const player1 = props.players[0]
            const player2 = props.players[1]
            expect(wrapper.find(Select).props().options).toEqual([
                {
                    value: player1.id,
                    label: player1.firstName + ' ' + player1.lastName + ', ' + player1.position
                },
                {
                    value: player2.id,
                    label: player2.firstName + ' ' + player2.lastName + ', ' + player2.position
                }
            ])
        })

        it('sets the keeper id as the select value', () => {
            const {props, wrapper} = setup()

            const select = wrapper.find(Select)
            expect(select.props().value).toEqual(props.member.keeper.id)
        })
    })

    describe('behavior', () => {
        it('calls select keeper action on keeper change for an identifier', () => {
            const {props, wrapper} = setup()

            wrapper.find(Select).props().onChange(1)

            const player = props.players[0]
            expect(props.select).lastCalledWith(
                1, {
                    keeper: {
                        id: player.id,
                        name: player.firstName + ' ' + player.lastName,
                        round: 0,
                        penalty: 0
                    }
                })
        })

        it('calls select keeper action on keeper round change for an identifier with the round', () => {
            const {props, wrapper} = setup()

            wrapper.find('select').props().onChange({target: {value: 9}})

            const player = props.players[0]
            expect(props.select).lastCalledWith(
                1, {
                    keeper: {
                        id: player.id,
                        name: player.firstName + ' ' + player.lastName,
                        round: 9,
                        penalty: 10
                    }
                }
            )
        })

        it('calls remove keeper action on change when change results in null value being passed', () => {
            const {props, wrapper} = setup()
            wrapper.find(Select).props().onChange(null)

            expect(props.remove).lastCalledWith(1, 'keeper')
        })
    })
})