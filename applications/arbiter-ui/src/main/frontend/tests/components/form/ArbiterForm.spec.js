import React from 'react'
import {shallow} from 'enzyme'
import configureStore from 'redux-mock-store'
import * as ReactTestUtils from 'react-dom/lib/ReactTestUtils'
import Connected, {ArbiterForm, mapDispatchToProps, mapStateToProps} from '../../../components/form/ArbiterForm'

describe('ArbiterForm', () => {
    describe('rendering', () => {
        it('connected component matches snapshot', () => {
            const mocked = configureStore()({app: {leagueMembers: []}})
            const rendered = ReactTestUtils.createRenderer()
                .render(<Connected store={mocked} fetchPlayers={jest.fn()}/>)

            expect(rendered).toMatchSnapshot()
        })

        it('component matches snapshot', () => {
            const rendered = ReactTestUtils.createRenderer()
                .render(<ArbiterForm fetchPlayers={jest.fn()} leagueMembers={[]}/>)

            expect(rendered).toMatchSnapshot()
        })
    })

    describe('property delegation', () => {
        describe('async actions', () => {
            it('maps dispatch to props', () => {
                const dispatch = jest.fn()
                const {fetchPlayers} = mapDispatchToProps(dispatch)

                fetchPlayers()
                expect(dispatch).toHaveBeenCalled()
            })
        })

        it('maps state to props', () => {
            const actual = mapStateToProps({app: {leagueMembers: []}})
            expect(actual).toEqual({leagueMembers: []})
        })

        it('can create league member components', () => {
            const rendered = shallow(
                <ArbiterForm
                    fetchPlayers={jest.fn()}
                    leagueMembers={[
                        {id: 1}, {id: 2}, {id: 3}
                    ]}/>)

            const members = rendered.find('Connect(LeagueMember)')
            expect(members.length).toEqual(3)
        })
    })
})