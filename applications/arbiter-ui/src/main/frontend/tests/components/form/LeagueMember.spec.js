import React from 'react'
import configureStore from 'redux-mock-store'
import ReactTestUtils from 'react-dom/lib/ReactTestUtils'
import Connected, {LeagueMember, mapStateToProps} from '../../../components/form/LeagueMember'

describe('LeagueMember', () => {
    const setup = () => {
        const props = {
            member: {id: 1},
            players: [{id: 1}, {id: 2}]
        }

        return {props}
    }

    describe('rendering', () => {
        it('connected component matches snapshot', () => {
            const {props} = setup()

            const mocked = configureStore()({app: {nfl: {players: props.players}}})
            const rendered = ReactTestUtils.createRenderer().render(<Connected store={mocked} member={props.member}/>)
            expect(rendered).toMatchSnapshot()
        })

        it('component matches snapshot', () => {
            const {props} = setup()

            const rendered = ReactTestUtils.createRenderer().render(<LeagueMember {...props} />)
            expect(rendered).toMatchSnapshot()
        })
    })

    describe('property delegation', () => {
        it('maps state to props', () => {
            const {props} = setup()

            const actual = mapStateToProps({app: {nfl: {players: props.players}}})
            expect(actual).toEqual({players: props.players})
        })
    })
})