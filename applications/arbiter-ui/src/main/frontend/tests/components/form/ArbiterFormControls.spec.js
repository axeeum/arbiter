import React from 'react'
import {shallow} from 'enzyme'
import renderer from 'react-test-renderer'
import configureStore from 'redux-mock-store'
import * as ReactTestUtils from 'react-dom/lib/ReactTestUtils'
import Connected, {
    ArbiterFormControls,
    mapStateToProps,
    mapDispatchToProps
} from '../../../components/form/ArbiterFormControls'
import {addLeagueMember} from '../../../state/app/actions'

describe('ArbiterFormControls', () => {
    const setup = () => {
        const props = {
            rounds: 10,
            add: jest.fn(),
            arbitrate: jest.fn(),
            leagueMembers: [{id: 1}, {id: 2}, {id: 3}]
        }

        return {
            props,
            wrapper: shallow(<ArbiterFormControls {...props} />)
        }
    }

    describe('rendering', () => {
        it('connected component matches snapshot', () => {
            const {props} = setup()
            const mocked = configureStore()({
                app: {
                    rounds: props.rounds,
                    leagueMembers: props.leagueMembers
                }
            })

            const shallowRenderer = ReactTestUtils.createRenderer()
            const rendered = shallowRenderer.render(
                <Connected
                    store={mocked}
                    add={props.add}
                    arbitrate={props.arbitrate}
                />)

            expect(rendered).toMatchSnapshot()
        })

        it('component matches snapshot', () => {
            const {props} = setup()
            const json = renderer.create(<ArbiterFormControls {...props} />).toJSON()

            expect(json).toMatchSnapshot()
        })
    })

    describe('property delegation', () => {
        it('matches synchronous dispatch to props', () => {
            const dispatch = jest.fn()
            const {add} = mapDispatchToProps(dispatch)

            add()
            expect(dispatch).lastCalledWith(addLeagueMember())
        })
    })

    describe('behavior', () => {
        it('triggers add league member on click', () => {
            const {props, wrapper} = setup()

            const addBtn = wrapper.find('button').at(1)
            addBtn.props().onClick()

            expect(props.add).toHaveBeenCalledWith()
        })
    })
})