import React from 'react'
import {shallow} from 'enzyme'
import configureStore from 'redux-mock-store'
import Connected, {PlayerName, mapDispatchToProps} from '../../../components/form/PlayerName'
import ReactTestUtils from 'react-dom/lib/ReactTestUtils'
import {updatePlayerName} from '../../../state/app/actions'

describe('PlayerName', () => {
    const setup = () => {
        const props = {
            member: {
                id: 1,
                name: 'a'
            },
            update: jest.fn()
        }

        return {props, wrapper: shallow(<PlayerName {...props} />)}
    }

    describe('rendering', () => {
        it('connected component matches snapshot', () => {
            const {props} = setup()
            const mocked = configureStore()({...props})

            const shallowRenderer = ReactTestUtils.createRenderer()
            const rendered = shallowRenderer.render(
                <Connected store={mocked} member={props.member}/>
            )

            expect(rendered).toMatchSnapshot()
        })

        it('component matches snapshot', () => {
            const {props} = setup()

            const rendered = ReactTestUtils.createRenderer().render(<PlayerName {...props} />)
            expect(rendered).toMatchSnapshot()
        })
    })

    describe('behavior', () => {
        it('calls update player name action on change', () => {
            const {props, wrapper} = setup()
            wrapper.find('input').simulate('change', {target: {value: 'bae'}})

            expect(props.update).lastCalledWith(1, 'bae')
        })
    })

    describe('property delegation', () => {
        it('maps dispatch to props', () => {
            const dispatch = jest.fn()
            const {update} = mapDispatchToProps(dispatch)

            update(1, 'bae')
            expect(dispatch).lastCalledWith(updatePlayerName(1, 'bae'))
        })
    })
})