import React from 'react'
import ReactTestUtils from 'react-dom/lib/ReactTestUtils'
import {shallow} from 'enzyme'
import configureStore from 'redux-mock-store'
import renderer from 'react-test-renderer'
import Up from 'react-icons/go/arrow-up'
import Down from 'react-icons/go/arrow-down'
import Connected, {RoundPicker, mapStateToProps, mapDispatchToProps} from '../../../components/form/RoundPicker'
import {increaseRounds, decreaseRounds} from '../../../state/app/actions'

const setup = () => {
    const props = {
        rounds: 5,
        increase: jest.fn(),
        decrease: jest.fn()
    }

    return {props, wrapper: shallow(<RoundPicker {...props} />)}
}

describe('RoundPicker', () => {
    describe('rendering', () => {
        it('connected component matches snapshot', () => {
            const {props} = setup()
            const mocked = configureStore()({app: {rounds: props.rounds}})

            const shallowRenderer = ReactTestUtils.createRenderer()
            const rendered = shallowRenderer.render(<Connected store={mocked}/>)

            expect(rendered).toMatchSnapshot()
        })

        it('component matches snapshot', () => {
            let {props} = setup()

            let json = renderer.create(<RoundPicker {...props}/>).toJSON()
            expect(json).toMatchSnapshot()
        })
    })

    describe('behavior', () => {
        it('maps state to props', () => {
            const actual = mapStateToProps({app: {rounds: 100}})
            expect(actual).toEqual({rounds: 100})
        })

        it('maps dispatch to props', () => {
            const dispatch = jest.fn()
            const {increase, decrease} = mapDispatchToProps(dispatch)

            increase()
            expect(dispatch).lastCalledWith(increaseRounds())

            decrease()
            expect(dispatch).lastCalledWith(decreaseRounds())
        })

        it('clicking Up icon calls increase', () => {
            const {props, wrapper} = setup()

            wrapper.find(Up).props().onClick()
            expect(props.increase).toHaveBeenCalled()
        })

        it('clicking Down icon calls decrease', () => {
            const {props, wrapper} = setup()

            wrapper.find(Down).props().onClick()
            expect(props.decrease).toHaveBeenCalled()
        })
    })
})