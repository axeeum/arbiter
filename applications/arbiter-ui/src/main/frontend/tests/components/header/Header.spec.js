import React from 'react'
import renderer from 'react-test-renderer'
import Header from '../../../components/header/Header'

describe('Header', () => {
    it('matches snapshot', () => {
        const component = renderer.create(<Header title="skilibop"/>)
        expect(component.toJSON()).toMatchSnapshot()
    })
})