import React from 'react'
import configureStore from 'redux-mock-store'
import ReactTestUtils from 'react-dom/lib/ReactTestUtils'
import Connected, {mapDispatchToProps, mapStateToProps, Draftboard} from '../../../components/draftboard/Draftboard'

describe('Draftboard', () => {
    const setup = () => {
        const props = {
            rounds: 10,
            leagueMembers: [{id: 1}, {id: 2}, {id: 3}],
            selections: [
                {id: 1, round: 1, position: 1},
                {id: 2, round: 1, position: 2},
                {id: 3, round: 1, position: 3}
            ],
            arbitrate: jest.fn(),
        }

        return {props}
    }

    describe('rendering', () => {
        it('connected component matches snapshot', () => {
            const {props} = setup()

            const mocked = configureStore()({
                app: {
                    rounds: props.rounds,
                    leagueMembers: props.leagueMembers,
                    draftboard: {selections: props.selections}
                }
            })

            const rendered = ReactTestUtils
                .createRenderer()
                .render(<Connected store={mocked}/>)
            expect(rendered).toMatchSnapshot()
        })

        it('component matches snapshot', () => {
            const {props} = setup()

            const rendered = ReactTestUtils
                .createRenderer()
                .render(<Draftboard {...props} />)
            expect(rendered).toMatchSnapshot()
        })
    })

    describe('property delegation', () => {
        describe('async actions', () => {
            it('maps dispatch to props', () => {
                const dispatch = jest.fn()
                const {arbitrate} = mapDispatchToProps(dispatch)

                arbitrate({})
                expect(dispatch).toHaveBeenCalled()
            })
        })

        it('maps state to props', () => {
            const {props} = setup()
            const actual = mapStateToProps({
                app: {
                    rounds: props.rounds,
                    leagueMembers: props.leagueMembers,
                    draftboard: {selections: props.selections}
                }
            })

            expect(actual).toEqual({
                rounds: props.rounds,
                leagueMembers: props.leagueMembers,
                selections: props.selections
            })
        })
    })
})