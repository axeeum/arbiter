import React from 'react'
import renderer from 'react-test-renderer'
import Round from '../../../components/draftboard/Round'

describe('Round', () => {

    describe('rendering', () => {
        it('component matches snapshot', () => {
            const props = {
                round: 10,
                selections: [<div key={1}>1</div>, <div key={2}>2</div>],
            }

            const component = renderer.create(<Round {...props} />)
            expect(component.toJSON()).toMatchSnapshot()
        })
    })
})