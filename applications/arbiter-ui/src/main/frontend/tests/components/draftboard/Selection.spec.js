import React from 'react'
import renderer from 'react-test-renderer'
import Selection from '../../../components/draftboard/Selection'

describe('Selection', () => {
    describe('rendering', () => {
        it('component matches snapshot', () => {
            const props = {
                round: 1,
                selection: {
                    id: 1,
                    leagueMember: {
                        name: 'bae'
                    },
                    position: 1,
                    pick: 'a'
                }
            }

            const component = renderer.create(<Selection {...props} />)
            expect(component.toJSON()).toMatchSnapshot()
        })
    })
})