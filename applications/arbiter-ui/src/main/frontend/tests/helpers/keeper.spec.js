import * as keeper from '../../helpers/keeper.js'

describe('keeper helper', () => {
    describe('penalize', () => {
        it('returns zero when round is zero', () => {
            expect(keeper.penalize(0, 0)).toEqual(0)
        })

        it('returns penalty (round+2) when penalty is less than or equal to total rounds', () => {
            expect(keeper.penalize(1, 3)).toEqual(3)
        })

        it('returns secondary when secondary is less than or equal to total rounds', () => {
            expect(keeper.penalize(2, 3)).toEqual(3)
        })

        it('returns round when it equals total rounds', () => {
            expect(keeper.penalize(3, 3)).toEqual(3)
        })
    })

    describe('isNull', () => {
        it('returns true when keeper is null', () => {
            expect(keeper.isNull(null)).toEqual(true)
        })

        it('returns false when keeper is not null', () => {
            expect(keeper.isNull({id: 1})).toEqual(false)
        })
    })

    describe('handleKeeper', () => {
        const setup = () => {
            return {
                rounds: 12,
                member: {id: 1, keeper: null},
                players: [{id: 1, firstName: 'bae', lastName: 'bae'}],
                identifier: 'keeper',
                select: jest.fn(),
                remove: jest.fn(),
            }
        }

        it('calls remove when player id is null', () => {
            const props = setup()

            const handle = keeper.handleKeeper(
                props.rounds, props.member, props.players, props.identifier, props.select, props.remove)

            const v = null
            handle(v)
            expect(props.remove).lastCalledWith(1, 'keeper')
            expect(props.select).not.toBeCalled()
        })

        it('calls select when player id is present and keeper is null', () => {
            const props = setup()

            const handle = keeper.handleKeeper(
                props.rounds, props.member, props.players, props.identifier, props.select, props.remove)

            handle(1)
            expect(props.select).lastCalledWith(1, {keeper: {id: 1, name: 'bae bae', round: 0, penalty: 0}})
            expect(props.remove).not.toBeCalled()
        })

        it('calls select when player id and keeper are present', () => {
            const props = setup()

            const handle = keeper.handleKeeper(
                props.rounds, {id: 1, keeper: {round: 10}}, props.players, props.identifier, props.select, props.remove)

            handle(1)
            expect(props.select).lastCalledWith(1, {keeper: {id: 1, name: 'bae bae', round: 10, penalty: 12}})
            expect(props.remove).not.toBeCalled()
        })
    })

    describe('handleExclusions', () => {
        it('calls select', () => {
            const select = jest.fn()

            keeper.handleExclusions(10, {id: 1, keeper: {id: 1}}, 'keeper', 1, select)
            expect(select).lastCalledWith(1, {keeper: {id: 1, round: 1, penalty: 3}})
        })
    })

    describe('setRoundForUndrafted', () => {
        it('returns null keeper if it is already null', () => {
            expect(keeper.setRoundForUndrafted(10, null, 1)).toEqual(null)
            expect(keeper.setRoundForUndrafted(10, null, 2)).toEqual(null)
        })

        it('returns keeper if it has a round that is already set', () => {
            expect(keeper.setRoundForUndrafted(10, {round: 5}, 1)).toEqual({round: 5})
        })

        it('returns keeper with rounds-2 if it is the first keeper', () => {
            expect(keeper.setRoundForUndrafted(10, {round: 0}, 1)).toEqual({round: 8})
        })

        it('returns keeper with rounds-1 if it is the second keeper', () => {
            expect(keeper.setRoundForUndrafted(10, {round: 0}, 2)).toEqual({round: 9})
        })
    })

    describe('options', () => {
        it('returns an array of value and label objects built from players', () => {
            const players = [
                {id: 1, firstName: 'a', lastName: 'a', position: 'a'},
                {id: 2, firstName: 'b', lastName: 'b', position: 'b'},
                {id: 3, firstName: 'c', lastName: 'c', position: 'c'},
            ]

            expect(keeper.options(players)).toEqual([
                {value: 1, label: 'a a, a'},
                {value: 2, label: 'b b, b'},
                {value: 3, label: 'c c, c'},
            ])
        })
    })
})