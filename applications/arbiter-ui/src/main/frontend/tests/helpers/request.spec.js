import * as api from '../../helpers/request'

describe('request', () => {
    const setup = () => {
        const props = {
            method: 'GET',
            credentials: 'same-origin',
            headers: new Headers({
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            })
        }

        return {props}
    }

    it('resolves a promise on success', () => {
        global.fetch.mockResponse(JSON.stringify({id: 1}), {'status': 200})

        const {props} = setup()
        api.request('api/mock', {method: 'GET'})
            .then((response) => {
                expect(response.status).toEqual(200)
                expect(response._bodyText).toEqual(JSON.stringify({id: 1}))
                expect(global.fetch).lastCalledWith(new Request('api/mock', {...props}))
            })
    })

    it('resolves a promise by default', () => {
        global.fetch.mockResponse(JSON.stringify({id: 1}), {'status': 200})

        const {props} = setup()
        api.request('api/mock', {method: 'GET'})
            .then((response) => {
                expect(response._bodyText).toEqual(JSON.stringify({id: 1}))
                expect(global.fetch).lastCalledWith(new Request('api/mock', {...props}))
            })
    })

    it('can build a default request', () => {
        const {props} = setup()
        const actual = api.build('api/mock', {method: 'GET'})
        const expected = new Request('api/mock', {...props})

        expect(actual).toEqual(expected)
    })
})