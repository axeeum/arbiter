import * as store from '../../helpers/store'

describe('store', () => {
    describe('reducer', () => {
        it('returns state when action type is not found', () => {
            const {createReducer} = store
            const actual = createReducer({id: 1}, {work: 'doit'})({id: 2}, {type: 'not found'})

            expect(actual).toEqual({id: 2})
        })

        it('returns handler value when found', () => {
            const {createReducer} = store
            const actual = createReducer({id: 1}, {work: (a) => { return a }})({id: 2}, {type: 'work'})

            expect(actual).toEqual({id:2})
        })
    })

    describe('builder', () => {
        it('builds middleware', () => {
            const {buildStore} = store

            expect(buildStore()).toBeDefined()
        })
    })
})