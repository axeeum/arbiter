import * as reducers from '../../../state/app/reducer'
import {ArbiterActionTypes} from '../../../state/app/actions'

describe('reducer', () => {
    describe('draftboard', () => {
        it('returns state and changes isFetching to true on request', () => {
            const {draftboard, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.LOAD_DRAFTBOARD_REQUEST,
                draftboard: {isFetching: true, selections: []}
            }
            const actual = draftboard({...initialState}, action)

            const expected = {
                ...initialState,
                draftboard: action.draftboard
            }

            expect(actual).toEqual(expected)
        })

        it('returns state, draftboard and changes isFetching to false on success', () => {
            const {draftboard, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.LOAD_DRAFTBOARD_SUCCESS,
                draftboard: {isFetching: false, selections: [{id: 1}]}
            }
            const actual = draftboard({...initialState}, action)
            const expected = {...initialState, draftboard: action.draftboard}

            expect(actual).toEqual(expected)
        })

        it('returns state, draftboard error and changes isFetching to false on failure', () => {
            const {draftboard, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.LOAD_DRAFTBOARD_FAILURE,
                draftboard: {isFetching: false, error: 'ohno'}
            }
            const actual = draftboard({...initialState}, action)
            const expected = {...initialState, draftboard: {...initialState.draftboard, ...action.draftboard}}

            expect(actual).toEqual(expected)
        })

        it('returns state when action type is not matched', () => {
            const {draftboard, initialState} = reducers
            const actual = draftboard({...initialState}, {type: 'not found'})

            expect(actual).toEqual(initialState)
        })
    })

    describe('members', () => {
        it('returns state and adds two league members on add', () => {
            const {members, initialState} = reducers
            const actual = members({...initialState}, {type: ArbiterActionTypes.ADD_LEAGUE_MEMBERS})

            expect(actual.leagueMembers.length).toEqual(8)
        })

        it('will not add league member if at capacity', () => {
            const {members, initialState} = reducers
            const state = {
                ...initialState,
                capacity: 6,
                leagueMembers: [{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}]
            }
            const actual = members(state, {type: ArbiterActionTypes.ADD_LEAGUE_MEMBERS})

            expect(actual.leagueMembers.length).toEqual(6)
        })

        it('returns state with the specified league members name updated', () => {
            const {members, initialState} = reducers
            const state = {...initialState}
            const actual = members(state, {
                type: ArbiterActionTypes.UPDATE_LEAGUE_MEMBER_NAME,
                memberId: 1,
                name: 'bae'
            })

            expect(actual.leagueMembers[0].name).toEqual('bae')
        })

        it('returns state when action type is not matched', () => {
            const {members, initialState} = reducers
            const actual = members({...initialState}, {type: 'not found'})

            expect(actual).toEqual(initialState)
        })
    })

    describe('keepers', () => {
        const member = (state, id) => {
            return state.leagueMembers.find((member) => {
                if (member.id === id) return member
            })
        }

        it('returns state where league member at id has an updated keeper on select', () => {
            const {keepers, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.SELECT_KEEPER,
                memberId: 1,
                keeper: {firstKeeper: {id: 1, name: 'keeper', round: 6, penalty: 8}}
            }

            const actual = keepers({...initialState}, action)
            const found = member(actual, action.memberId)

            expect(found.firstKeeper).toEqual(action.keeper.firstKeeper)
        })

        it('returns state minus the keeper from the league member', () => {
            const {keepers, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.REMOVE_KEEPER,
                memberId: 1,
                identifier: 'firstKeeper'
            }

            const state = {
                ...initialState,
                leagueMembers: [
                    {...initialState.leagueMembers[0], firstKeeper: {id: 1, name: 'keeper', round: 1, penalty: 3}},
                    ...initialState.leagueMembers.slice(1)
                ]
            }
            const actual = keepers(state, action)
            const found = member(actual, action.memberId)

            expect(found.firstKeeper).toEqual(null)
        })

        it('returns state when action type is not matched', () => {
            const {keepers, initialState} = reducers
            const actual = keepers({...initialState}, {type: 'not found'})

            expect(actual).toEqual(initialState)
        })
    })

    describe('rounds', () => {
        it('returns state with rounds increased by one on increase', () => {
            const {rounds, initialState} = reducers
            const actual = rounds({...initialState}, {type: ArbiterActionTypes.INCREASE_ROUNDS})

            expect(actual.rounds).toEqual(7)
        })

        it('does not allow rounds to exceed maximum', () => {
            const {rounds, initialState} = reducers
            const actual = rounds({
                ...initialState,
                rounds: 10,
                maximum: 10
            }, {type: ArbiterActionTypes.INCREASE_ROUNDS})

            expect(actual.rounds).toEqual(10)
        })

        it('returns state with rounds decreased by one on decrease', () => {
            const {rounds, initialState} = reducers
            const actual = rounds({...initialState, rounds: 10}, {type: ArbiterActionTypes.DECREASE_ROUNDS})

            expect(actual.rounds).toEqual(9)
        })

        it('does not allow rounds to precede minimum', () => {
            const {rounds, initialState} = reducers
            const actual = rounds({
                ...initialState,
                rounds: 6,
                minimum: 6
            }, {type: ArbiterActionTypes.DECREASE_ROUNDS})

            expect(actual.rounds).toEqual(6)
        })

        it('returns state when action type is not matched', () => {
            const {rounds, initialState} = reducers
            const actual = rounds({...initialState}, {type: 'not found'})

            expect(actual).toEqual(initialState)
        })
    })

    describe('nflPlayers', () => {
        it('returns state and changes isFetching to true on request ', () => {
            const {nflPlayers, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.LOAD_NFL_PLAYERS_REQUEST,
                nfl: {isFetching: true}
            }
            const actual = nflPlayers({...initialState}, action)

            expect(actual).toEqual({...initialState, nfl: {...action.nfl, players: []}})
        })

        it('returns state, nfl players and changes isFetching to false on success', () => {
            const {nflPlayers, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.LOAD_NFL_PLAYERS_SUCCESS,
                nfl: {
                    isFetching: false,
                    players: []
                }
            }
            const actual = nflPlayers({...initialState}, action)

            expect(actual).toEqual({...initialState, nfl: {...action.nfl}})
        })

        it('returns state, nfl error and changes isFetching to false on failure', () => {
            const {nflPlayers, initialState} = reducers
            const action = {
                type: ArbiterActionTypes.LOAD_NFL_PLAYERS_FAILURE,
                nfl: {
                    isFetching: false,
                    error: ['ohno']
                }
            }
            const actual = nflPlayers({...initialState}, action)

            expect(actual).toEqual({...initialState, nfl: {...action.nfl, players: []}})
        })

        it('returns state when action type is not matched', () => {
            const {nflPlayers, initialState} = reducers
            const actual = nflPlayers({...initialState}, {type: 'not found'})

            expect(actual).toEqual(initialState)
        })
    })
})