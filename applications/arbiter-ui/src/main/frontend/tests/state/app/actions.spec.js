import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'
import * as actions from '../../../state/app/actions'

describe('actions', () => {
    describe('synchronous', () => {
        it('returns add league member action', () => {
            expect(actions.addLeagueMember())
                .toEqual({type: actions.ArbiterActionTypes.ADD_LEAGUE_MEMBERS})
        })

        it('returns increase round action', () => {
            expect(actions.increaseRounds())
                .toEqual({type: actions.ArbiterActionTypes.INCREASE_ROUNDS})
        })

        it('returns decrease round action', () => {
            expect(actions.decreaseRounds())
                .toEqual({type: actions.ArbiterActionTypes.DECREASE_ROUNDS})
        })

        it('returns select keeper action', () => {
            expect(actions.selectKeeper(1, {id: 1}))
                .toEqual({type: actions.ArbiterActionTypes.SELECT_KEEPER, memberId: 1, keeper: {id: 1}})
        })

        it('returns remove keeper action', () => {
            expect(actions.removeKeeper(1, 'identifier'))
                .toEqual({type: actions.ArbiterActionTypes.REMOVE_KEEPER, memberId: 1, identifier: 'identifier'})
        })

        it('returns update player name action', () => {
            expect(actions.updatePlayerName(1, 'a'))
                .toEqual({type:actions.ArbiterActionTypes.UPDATE_LEAGUE_MEMBER_NAME, memberId: 1, name: 'a'})
        })
    })

    describe('asynchronous', () => {
        const setup = () => {
            const props = {nfl: {players: [{id: 1}]}}
            const requestdefaults = {
                credentials: 'same-origin',
                headers: new Headers({
                    'Accept': 'application/json; charset=utf-8',
                    'Content-Type': 'application/json; charset=utf-8'
                })
            }

            const mockstore = configureStore([thunk])

            return {props, requestdefaults, mockstore}
        }

        it('calls fetch players chain successfully', () => {
            const expected = [
                {
                    type: actions.ArbiterActionTypes.LOAD_NFL_PLAYERS_REQUEST,
                    nfl: {isFetching: true}
                },
                {
                    type: actions.ArbiterActionTypes.LOAD_NFL_PLAYERS_SUCCESS,
                    nfl: {isFetching: false, players: [{id: 1}]}
                }
            ]

            global.fetch.mockResponse(JSON.stringify({players: [{id: 1}]}), {'status': 200})

            const {mockstore, requestdefaults} = setup()
            const store = mockstore({app: {nfl: {players: [{id: 1}]}}})
            return store.dispatch(actions.fetchPlayers())
                .then(() => {
                    const request = new Request(
                        'nfl/top100',
                        {
                            method: 'GET',
                            ...requestdefaults
                        }
                    )
                    expect(global.fetch).lastCalledWith(request)
                    expect(store.getActions()).toEqual(expected)
                })
        })

        it('calls fetch players chain unsuccessfully', () => {
            const expected = [
                {
                    type: actions.ArbiterActionTypes.LOAD_NFL_PLAYERS_REQUEST,
                    nfl: {isFetching: true}
                },
                {
                    type: actions.ArbiterActionTypes.LOAD_NFL_PLAYERS_FAILURE,
                    nfl: {isFetching: false, error: new Error('unsupported BodyInit type')}
                }
            ]

            global.fetch.mockResponse({}, {'status': 400, 'statusText': '400'})
            const {props, mockstore} = setup()
            const store = mockstore({app: {...props}})
            return store.dispatch(actions.fetchPlayers())
                .then(() => {
                    expect(store.getActions()).toEqual(expected)
                })
        })

        it('calls arbitrate draft chain successfully', () => {
            const expected = [
                {
                    type: actions.ArbiterActionTypes.LOAD_DRAFTBOARD_REQUEST,
                    draftboard: {isFetching: true}
                },
                {
                    type: actions.ArbiterActionTypes.LOAD_DRAFTBOARD_SUCCESS,
                    draftboard: {
                        isFetching: false,
                        selections: [{id: 1}]
                    }
                }
            ]

            global.fetch.mockResponse(JSON.stringify({selections: [{id: 1}]}), {'status': 200})

            const {mockstore, requestdefaults} = setup()
            const store = mockstore({app: {draftboard: {selections: [{id: 1}]}}})
            const league = {rounds: 1, members: []}
            return store.dispatch(actions.arbitrateDraft(league))
                .then(() => {
                    const request = new Request(
                        'api/draft/arbitrate',
                        {
                            method: 'POST',
                            body: JSON.stringify(league),
                            ...requestdefaults
                        }
                    )
                    expect(global.fetch).lastCalledWith(request)
                    expect(store.getActions()).toEqual(expected)
                })
        })

        it('calls arbitrate draft chain unsuccessfully', () => {
            const expected = [
                {
                    type: actions.ArbiterActionTypes.LOAD_DRAFTBOARD_REQUEST,
                    draftboard: {isFetching: true}
                },
                {
                    type: actions.ArbiterActionTypes.LOAD_DRAFTBOARD_FAILURE,
                    draftboard: {
                        isFetching: false,
                        error: new Error('unsupported BodyInit type')
                    }
                }
            ]

            global.fetch.mockResponse({}, {'status': 400, 'statusText': '400'})

            const {mockstore} = setup()
            const store = mockstore({app: {draftboard: {selections: []}}})
            return store.dispatch(actions.arbitrateDraft())
                .then(() => {
                    expect(store.getActions()).toEqual(expected)
                })
        })
    })

    it('has an expected amount of constants', () => {
        expect(Object.keys(actions.ArbiterActionTypes).length).toEqual(12) //does this really help
    })
})