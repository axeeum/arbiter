import React from 'react'
import {Route, IndexRoute} from 'react-router'
import Draftboard from './components/draftboard/Draftboard'
import ArbiterForm from './components/form/ArbiterForm'
import PageNotFound from './containers/PageNotFound'
import 'script!jquery'
import 'script!foundation-sites'
import './main/_foundation.scss'

export default (
    <Route path="/">
        <IndexRoute component={ArbiterForm}/>
        <Route path="/draftboard" component={Draftboard}/>
        <Route path="*" component={PageNotFound}/>
    </Route>
)