export const penalize = (round, rounds) => {
    if (round === 0)
        return 0

    const penalty = round + 2
    const secondary = round + 1

    if (penalty <= rounds) {
        return penalty
    } else if (penalty > rounds) {
        if (secondary <= rounds) {
            return secondary
        } else {
            return round
        }
    }
}

export const isNull = (keeper) => {
    return keeper == null
}

export const handleKeeper = (rounds, member, players, identifier, select, remove) => {
    return function (value) {
        if (value == null) {
            remove(member.id, identifier)
        } else {
            const player = players.find((player) => {
                return player.id == value
            })

            const keeper = member[identifier]
            let round = !isNull(keeper) ? keeper.round : 0
            select(
                member.id,
                {
                    [identifier]: {
                        id: value,
                        name: player.firstName + ' ' + player.lastName,
                        round: Number(round),
                        penalty: penalize(round, rounds)
                    }
                })
        }
    }.bind(this);
}

export const handleExclusions = (rounds, member, identifier, value, select) => {
    const round = Number(value)
    select(member.id, {
        [identifier]: {
            ...member[identifier],
            round: round,
            penalty: penalize(round, rounds)
        }
    })
}

export const setRoundForUndrafted = (rounds, keeper, identifier) => {
    if (keeper != null && keeper.round === 0) {
        if (identifier === 1)
            keeper.round = Number(rounds - 2)

        if (identifier == 2)
            keeper.round = Number(rounds - 1)
    }

    return keeper
}

export const options = (players) => {
    return players.map(player => {
        return {
            value: player.id,
            label: player.firstName + ' ' + player.lastName + ', ' + player.position
        }
    })
}