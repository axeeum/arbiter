export function request(url, init) {
    return new Promise((resolve, reject) => {
        fetch(build(url, init))
            .then(response => {
                switch (response.status) {
                    case 200:
                        return resolve(response)
                }
            })
            .catch(err => reject(err))
    });
}

export function build(path, init) {
    return new Request(path, {
        ...init,
        credentials: 'same-origin',
        headers: new Headers({
            'Accept': 'application/json; charset=utf-8',
            'Content-Type': 'application/json; charset=utf-8'
        })
    })
}