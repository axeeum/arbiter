import {createReducer} from '../../helpers/store'
import {ArbiterActionTypes} from './actions'
import * as _ from 'lodash'

const leagueMember = () => {
    return {id: Number(_.uniqueId()), name: null, firstKeeper: null, secondKeeper: null}
}

export const initialState = {
    rounds: 6,
    minimum: 6,
    maximum: 13,
    capacity: 12,
    leagueMembers: [leagueMember(), leagueMember(), leagueMember(), leagueMember(), leagueMember(), leagueMember()],
    nfl: {isFetching: false, players: []},
    draftboard: {isFetching: false, selections: []},
}

const memberAtIdx = (state, action) => {
    return state.leagueMembers.findIndex((member) => {
        return member.id === action.memberId
    })
}

export function draftboard(state = initialState.draftboard, action) {
    switch (action.type) {
        case ArbiterActionTypes.LOAD_DRAFTBOARD_REQUEST:
            delete action.type

            return { ...state, draftboard: {...state.draftboard, ...action.draftboard}}
        case ArbiterActionTypes.LOAD_DRAFTBOARD_SUCCESS:
            delete action.type

            return { ...state, draftboard: {...state.draftboard, ...action.draftboard}}
        case ArbiterActionTypes.LOAD_DRAFTBOARD_FAILURE:
            delete action.type

            return { ...state, draftboard: {...state.draftboard, ...action.draftboard}}
        default:
            return state
    }
}

export function members(state = initialState.leagueMembers, action) {
    switch (action.type) {
        case ArbiterActionTypes.ADD_LEAGUE_MEMBERS:
            const leagueMembers = (state.leagueMembers.length + 2) <= state.capacity ?
                [...state.leagueMembers, leagueMember(), leagueMember()] : state.leagueMembers

            return {...state, leagueMembers}
        case ArbiterActionTypes.UPDATE_LEAGUE_MEMBER_NAME:
            const updater = memberAtIdx(state, action)

            return {
                ...state,
                leagueMembers: [
                    ...state.leagueMembers.slice(0, updater),
                    {...state.leagueMembers[updater], name: action.name},
                    ...state.leagueMembers.slice(updater + 1),
                ]
            }
        default:
            return state
    }
}

export function keepers(state = initialState.leagueMembers, action) {
    switch (action.type) {
        case ArbiterActionTypes.SELECT_KEEPER:
            const selector = memberAtIdx(state, action)
            return {
                ...state,
                leagueMembers: [
                    ...state.leagueMembers.slice(0, selector),
                    {...state.leagueMembers[selector], ...action.keeper},
                    ...state.leagueMembers.slice(selector + 1),
                ]
            }
        case ArbiterActionTypes.REMOVE_KEEPER:
            const remover = memberAtIdx(state, action)
            return {
                ...state,
                leagueMembers: [
                    ...state.leagueMembers.slice(0, remover),
                    {...state.leagueMembers[remover], [action.identifier]: null},
                    ...state.leagueMembers.slice(remover + 1),
                ]
            }
        default:
            return state
    }
}

export function rounds(state = initialState.rounds, action) {
    switch (action.type) {
        case ArbiterActionTypes.INCREASE_ROUNDS:
            let increasing = state.rounds
            if (++increasing <= state.maximum)
                return {...state, rounds: increasing}
            else
                return state
        case ArbiterActionTypes.DECREASE_ROUNDS:
            let decreasing = state.rounds
            if (--decreasing >= state.minimum)
                return {...state, rounds: decreasing}
            else
                return state
        default:
            return state
    }
}

export function nflPlayers(state = initialState.nfl, action) {
    switch (action.type) {
        case ArbiterActionTypes.LOAD_NFL_PLAYERS_REQUEST:
            return {
                ...state,
                nfl: {
                    ...state.nfl,
                    ...action.nfl
                }
            }
        case ArbiterActionTypes.LOAD_NFL_PLAYERS_SUCCESS:
            return {
                ...state,
                nfl: {
                    ...state.nfl,
                    ...action.nfl
                }
            }
        case ArbiterActionTypes.LOAD_NFL_PLAYERS_FAILURE:
            return {
                ...state,
                nfl: {
                    ...state.nfl,
                    ...action.nfl
                }
            }
        default:
            return state
    }
}

export const appReducer = createReducer(initialState, {
    [ArbiterActionTypes.SELECT_KEEPER]: keepers,
    [ArbiterActionTypes.REMOVE_KEEPER]: keepers,
    [ArbiterActionTypes.INCREASE_ROUNDS]: rounds,
    [ArbiterActionTypes.DECREASE_ROUNDS]: rounds,
    [ArbiterActionTypes.ADD_LEAGUE_MEMBERS]: members,
    [ArbiterActionTypes.UPDATE_LEAGUE_MEMBER_NAME]: members,
    [ArbiterActionTypes.LOAD_DRAFTBOARD_REQUEST]: draftboard,
    [ArbiterActionTypes.LOAD_DRAFTBOARD_SUCCESS]: draftboard,
    [ArbiterActionTypes.LOAD_DRAFTBOARD_FAILURE]: draftboard,
    [ArbiterActionTypes.LOAD_NFL_PLAYERS_REQUEST]: nflPlayers,
    [ArbiterActionTypes.LOAD_NFL_PLAYERS_SUCCESS]: nflPlayers,
    [ArbiterActionTypes.LOAD_NFL_PLAYERS_FAILURE]: nflPlayers,
})