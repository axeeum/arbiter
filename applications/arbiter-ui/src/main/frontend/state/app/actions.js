import {request} from '../../helpers/request'

export const ArbiterActionTypes = {
    LOAD_DRAFTBOARD_REQUEST: 'LOAD_DRAFTBOARD_REQUEST',
    LOAD_DRAFTBOARD_SUCCESS: 'LOAD_DRAFTBOARD_SUCCESS',
    LOAD_DRAFTBOARD_FAILURE: 'LOAD_DRAFTBOARD_FAILURE',
    LOAD_NFL_PLAYERS_REQUEST: 'LOAD_NFL_PLAYERS_REQUEST',
    LOAD_NFL_PLAYERS_SUCCESS: 'LOAD_NFL_PLAYERS_SUCCESS',
    LOAD_NFL_PLAYERS_FAILURE: 'LOAD_NFL_PLAYERS_FAILURE',
    UPDATE_LEAGUE_MEMBER_NAME: 'UPDATE_LEAGUE_MEMBER_NAME',
    ADD_LEAGUE_MEMBERS: 'ADD_LEAGUE_MEMBERS',
    INCREASE_ROUNDS: 'INCREASE_ROUNDS',
    DECREASE_ROUNDS: 'DECREASE_ROUNDS',
    SELECT_KEEPER: 'SELECT_KEEPER',
    REMOVE_KEEPER: 'REMOVE_KEEPER',
}

export function updatePlayerName(id, name) {
    return {type: ArbiterActionTypes.UPDATE_LEAGUE_MEMBER_NAME, memberId: id, name: name}
}

export function addLeagueMember() {
    return {type: ArbiterActionTypes.ADD_LEAGUE_MEMBERS}
}

export function increaseRounds() {
    return {type: ArbiterActionTypes.INCREASE_ROUNDS}
}

export function decreaseRounds() {
    return {type: ArbiterActionTypes.DECREASE_ROUNDS}
}

export function selectKeeper(id, keeper) {
    return {type: ArbiterActionTypes.SELECT_KEEPER, memberId: id, keeper: keeper}
}

export function removeKeeper(id, identifier) {
    return {type: ArbiterActionTypes.REMOVE_KEEPER, memberId: id, identifier: identifier}
}

export function loadDraftboardRequest() {
    return {type: ArbiterActionTypes.LOAD_DRAFTBOARD_REQUEST, draftboard: {isFetching: true}}
}

export function loadDraftboardSuccess(json) {
    return {type: ArbiterActionTypes.LOAD_DRAFTBOARD_SUCCESS, draftboard: {isFetching: false, ...json}}
}

export function loadDraftboardFailure(json) {
    return {
        type: ArbiterActionTypes.LOAD_DRAFTBOARD_FAILURE,
        draftboard: {isFetching: false, error: json}
    }
}

export function loadNflPlayersRequest() {
    return {type: ArbiterActionTypes.LOAD_NFL_PLAYERS_REQUEST, nfl: {isFetching: true}}
}

export function loadNflPlayersSuccess(players) {
    return {type: ArbiterActionTypes.LOAD_NFL_PLAYERS_SUCCESS, nfl: {isFetching: false, ...players}}
}

export function loadNflPlayersFailure(json) {
    return {type: ArbiterActionTypes.LOAD_NFL_PLAYERS_FAILURE, nfl: {isFetching: false, error: json}}
}

export function fetchPlayers() {
    const init = {method: 'GET'}

    return dispatch => {
        dispatch(loadNflPlayersRequest())
        return request('nfl/top100', init)
            .then(response => response.json())
            .then(json => dispatch(loadNflPlayersSuccess(json)))
            .catch(json => dispatch(loadNflPlayersFailure(json)))
    }
}

export function arbitrateDraft(league) {
    const init = {
        method: 'POST',
        body: JSON.stringify(league)
    }

    return dispatch => {
        dispatch(loadDraftboardRequest())
        return request('api/draft/arbitrate', init)
            .then(response => response.json())
            .then(json => dispatch(loadDraftboardSuccess(json)))
            .catch(json => dispatch(loadDraftboardFailure(json)))
    }
}