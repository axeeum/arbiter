package charleston.fantasy.services;


import charleston.fantasy.models.Nfl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class NflApiService {

    private final Path path;
    private final ObjectMapper mapper;

    @Autowired
    public NflApiService(Path path, ObjectMapper mapper) {
        this.path = path;
        this.mapper = mapper;
    }

    public Nfl getTop100() throws IOException, URISyntaxException {
        String content = new String(Files.readAllBytes(path));
        return mapper.readValue(content, Nfl.class);
    }
}