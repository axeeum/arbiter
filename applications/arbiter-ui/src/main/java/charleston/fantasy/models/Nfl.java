package charleston.fantasy.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Nfl {
    private final List<Player> players;

    @JsonCreator
    public Nfl(
        @JsonProperty("players") List<Player> players
    ) {
        this.players = players;
    }


    public List<Player> getPlayers() {
        return players;
    }

    @Override
    public String toString() {
        return "Nfl{" +
            "players=" + players +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Nfl nfl = (Nfl) o;

        return players != null ? players.equals(nfl.players) : nfl.players == null;
    }

    @Override
    public int hashCode() {
        return players != null ? players.hashCode() : 0;
    }
}
