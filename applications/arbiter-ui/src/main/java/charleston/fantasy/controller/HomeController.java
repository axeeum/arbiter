package charleston.fantasy.controller;

import charleston.fantasy.models.Nfl;
import charleston.fantasy.services.NflApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

import static java.util.Collections.singletonList;

@Controller
public class HomeController {

    private Logger logger = LoggerFactory.getLogger(HomeController.class);

    private String api;
    private RestTemplate client;
    private NflApiService nflApiService;

    @Autowired
    public HomeController(
        String api,
        RestTemplate client,
        NflApiService nflApiService
    ) {
        this.api = api;
        this.client = client;
        this.nflApiService = nflApiService;
    }

    @RequestMapping(value = "/")
    public String home() {
        return "index";
    }

    @RequestMapping(
        method = RequestMethod.POST,
        value = "/api/**",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> posts(
        HttpServletRequest rqst,
        @RequestBody String body
    ) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.setAccept(singletonList(MediaType.APPLICATION_JSON_UTF8));

        ResponseEntity<String> apiResponse = client.postForEntity(
            api + rqst.getRequestURI(),
            new HttpEntity<>(body, headers),
            String.class
        );

        return new ResponseEntity<>(apiResponse.getBody(), apiResponse.getStatusCode());
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = "/nfl/top100",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Nfl> top100() throws IOException, URISyntaxException {
        return new ResponseEntity<>(nflApiService.getTop100(), HttpStatus.OK);
    }
}