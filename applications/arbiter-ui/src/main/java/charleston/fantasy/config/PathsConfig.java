package charleston.fantasy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
public class PathsConfig {

    @Bean
    public Path path() throws URISyntaxException {
        return Paths.get(getClass().getResource("/players.json").toURI());
    }
}