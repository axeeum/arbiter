package charleston.fantasy.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

@Configuration
public class ArbiterUiConfig {

    @Bean
    @Value("${arbiter.api}")
    public String arbiterApi(String api) {
        return api;
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate template = new RestTemplate();
        template.getInterceptors().add(new BasicAuthorizationInterceptor("admin", "admin"));
        template.getMessageConverters().add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        return template;
    }
}