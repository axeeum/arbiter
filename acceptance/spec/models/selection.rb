class Selection
  def initialize(round, league_member, position, pick = nil)
    @round = round
    @leagueMember = league_member
    @pick = pick
    @position = position
  end
end