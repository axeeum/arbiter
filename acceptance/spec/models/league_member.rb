class LeagueMember
  def initialize(id, name, first_keeper = nil, second_keeper = nil)
    @id = id
    @name = name
    @firstKeeper = first_keeper
    @secondKeeper = second_keeper
  end
end