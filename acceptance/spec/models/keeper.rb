class Keeper
  def initialize(id, name, round, penalty = 0)
    @id = id
    @name = name
    @round = round
    @penalty = penalty
  end
end