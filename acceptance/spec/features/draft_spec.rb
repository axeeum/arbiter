require_relative '../spec_helper'

describe 'draft api' do
  it 'returns 200' do
    actual = post '/api/draft/arbitrate', { :rounds => 1, :leagueMembers => [] }.to_json
    expect(actual.code).to eq 200
  end

  describe('returns the draftboard with (round+2) penalty and fifo compensatory picks') do
    let(:rl) { LeagueMember.new(1, 'RL', Keeper.new(1, 'Rob Gronkowski', 3, 5)) }
    let(:em) { LeagueMember.new(2, 'EM', Keeper.new(2, 'Randall Cobb', 3, 5), Keeper.new(3, 'Carlos Hyde', 9, 11)) }
    let(:jc) { LeagueMember.new(3, 'JC', Keeper.new(4, 'Brandin Cooks', 6, 8), Keeper.new(5, 'Ben Roethlesberger', 12, 13)) }
    let(:rh) { LeagueMember.new(4, 'RH') }
    let(:mc) { LeagueMember.new(5, 'MC', Keeper.new(6, 'Mike Evans', 11, 13), Keeper.new(7, 'Teddy Bridgewater', 10)) }
    let(:bk) { LeagueMember.new(6, 'BK', Keeper.new(8, 'Eddie Lacy', 5, 7), Keeper.new(9, 'Odell Beckham', 11)) }
    let(:jl) { LeagueMember.new(7, 'JL', Keeper.new(10, 'Greg Olsen', 11)) }
    let(:sm) { LeagueMember.new(8, 'SM', Keeper.new(11, 'CJ Anderson', 11)) }
    let(:dh) { LeagueMember.new(9, 'DH', Keeper.new(12, 'Allen Robinson', 11)) }
    let(:bs) { LeagueMember.new(10, 'BS', Keeper.new(13, 'Jarvis Landry', 11), Keeper.new(14, 'Jeremy Hill', 13)) }
    let(:jt) { LeagueMember.new(11, 'JT', Keeper.new(15, 'Russel Wilson', 9, 11)) }
    let(:rc) { LeagueMember.new(12, 'RC', Keeper.new(16, 'Antonio Brown', 2, 4)) }

    it 'reflects 2015 12 league member 13 round league' do
      initial_draft_board = read_fixture '2015_draft_order.json'
      actual = post '/api/draft/arbitrate', initial_draft_board

      expected = {
        :selections => [
          Selection.new(1, rl, 1),
          Selection.new(1, em, 2),
          Selection.new(1, jc, 3),
          Selection.new(1, rh, 4),
          Selection.new(1, mc, 5),
          Selection.new(1, bk, 6),
          Selection.new(1, jl, 7),
          Selection.new(1, sm, 8),
          Selection.new(1, dh, 9),
          Selection.new(1, bs, 10),
          Selection.new(1, jt, 11),
          Selection.new(1, rc, 12),

          Selection.new(2, rc, 13, 'Antonio Brown'),
          Selection.new(2, jt, 14),
          Selection.new(2, bs, 15),
          Selection.new(2, dh, 16),
          Selection.new(2, sm, 17),
          Selection.new(2, jl, 18),
          Selection.new(2, bk, 19),
          Selection.new(2, mc, 20),
          Selection.new(2, rh, 21),
          Selection.new(2, jc, 22),
          Selection.new(2, em, 23),
          Selection.new(2, rl, 24),

          Selection.new(3, rl, 25, 'Rob Gronkowski'),
          Selection.new(3, em, 26, 'Randall Cobb'),
          Selection.new(3, jc, 27),
          Selection.new(3, rh, 28),
          Selection.new(3, mc, 29),
          Selection.new(3, bk, 30),
          Selection.new(3, jl, 31),
          Selection.new(3, sm, 32),
          Selection.new(3, dh, 33),
          Selection.new(3, bs, 34),
          Selection.new(3, jt, 35),
          Selection.new(3, rc, 36),

          Selection.new(4, jt, 37),
          Selection.new(4, bs, 38),
          Selection.new(4, dh, 39),
          Selection.new(4, sm, 40),
          Selection.new(4, jl, 41),
          Selection.new(4, bk, 42),
          Selection.new(4, mc, 43),
          Selection.new(4, rh, 44),
          Selection.new(4, jc, 45),
          Selection.new(4, em, 46),
          Selection.new(4, rl, 47),
          Selection.new(4, jc, 48),

          Selection.new(5, rh, 49),
          Selection.new(5, mc, 50),
          Selection.new(5, bk, 51, 'Eddie Lacy'),
          Selection.new(5, jl, 52),
          Selection.new(5, sm, 53),
          Selection.new(5, dh, 54),
          Selection.new(5, bs, 55),
          Selection.new(5, jt, 56),
          Selection.new(5, rc, 57),
          Selection.new(5, rc, 58),
          Selection.new(5, jt, 59),
          Selection.new(5, bs, 60),

          Selection.new(6, dh, 61),
          Selection.new(6, sm, 62),
          Selection.new(6, jl, 63),
          Selection.new(6, bk, 64),
          Selection.new(6, mc, 65),
          Selection.new(6, rh, 66),
          Selection.new(6, jc, 67, 'Brandin Cooks'),
          Selection.new(6, em, 68),
          Selection.new(6, rl, 69),
          Selection.new(6, rl, 70),
          Selection.new(6, em, 71),
          Selection.new(6, jc, 72),

          Selection.new(7, rh, 73),
          Selection.new(7, mc, 74),
          Selection.new(7, jl, 75),
          Selection.new(7, sm, 76),
          Selection.new(7, dh, 77),
          Selection.new(7, bs, 78),
          Selection.new(7, jt, 79),
          Selection.new(7, rc, 80),
          Selection.new(7, rc, 81),
          Selection.new(7, jt, 82),
          Selection.new(7, bs, 83),
          Selection.new(7, dh, 84),

          Selection.new(8, sm, 85),
          Selection.new(8, jl, 86),
          Selection.new(8, bk, 87),
          Selection.new(8, mc, 88),
          Selection.new(8, rh, 89),
          Selection.new(8, em, 90),
          Selection.new(8, rl, 91),
          Selection.new(8, rl, 92),
          Selection.new(8, em, 93, 'Carlos Hyde'),
          Selection.new(8, jc, 94),
          Selection.new(8, rh, 95),
          Selection.new(8, mc, 96),

          Selection.new(9, bk, 97),
          Selection.new(9, jl, 98),
          Selection.new(9, sm, 99),
          Selection.new(9, dh, 100),
          Selection.new(9, bs, 101),
          Selection.new(9, jt, 102, 'Russel Wilson'),
          Selection.new(9, rc, 103),
          Selection.new(9, rc, 104),
          Selection.new(9, jt, 105),
          Selection.new(9, bs, 106),
          Selection.new(9, dh, 107),
          Selection.new(9, sm, 108),

          Selection.new(10, jl, 109),
          Selection.new(10, bk, 110),
          Selection.new(10, mc, 111, 'Teddy Bridgewater'),
          Selection.new(10, rh, 112),
          Selection.new(10, jc, 113),
          Selection.new(10, em, 114),
          Selection.new(10, rl, 115),
          Selection.new(10, rl, 116),
          Selection.new(10, jc, 117),
          Selection.new(10, rh, 118),
          Selection.new(10, mc, 119, 'Mike Evans'),
          Selection.new(10, bk, 120, 'Odell Beckham'),

          Selection.new(11, jl, 121, 'Greg Olsen'),
          Selection.new(11, sm, 122, 'CJ Anderson'),
          Selection.new(11, dh, 123, 'Allen Robinson'),
          Selection.new(11, bs, 124, 'Jarvis Landry'),
          Selection.new(11, rc, 125),
          Selection.new(11, rc, 126),
          Selection.new(11, jt, 127),
          Selection.new(11, bs, 128),
          Selection.new(11, dh, 129),
          Selection.new(11, sm, 130),
          Selection.new(11, jl, 131),
          Selection.new(11, bk, 132),

          Selection.new(12, mc, 133),
          Selection.new(12, rh, 134),
          Selection.new(12, jc, 135, 'Ben Roethlesberger'),
          Selection.new(12, em, 136),
          Selection.new(12, rl, 137),
          Selection.new(12, rl, 138),
          Selection.new(12, em, 139),
          Selection.new(12, rh, 140),
          Selection.new(12, bk, 141),
          Selection.new(12, jl, 142),
          Selection.new(12, sm, 143),
          Selection.new(12, dh, 144),

          Selection.new(13, bs, 145, 'Jeremy Hill'),
          Selection.new(13, jt, 146),
          Selection.new(13, rc, 147),
          Selection.new(13, rc, 148),
          Selection.new(13, rl, 149),
          Selection.new(13, em, 150),
          Selection.new(13, bk, 151),
          Selection.new(13, jc, 152),
          Selection.new(13, em, 153),
          Selection.new(13, jt, 154),
          Selection.new(13, jc, 155),
          Selection.new(13, mc, 156),
        ]
      }

      expect(JSON.parse(actual.body)).to eq JSON.parse(expected.to_json)
    end
  end
end