require 'rubygems'
require 'colorized_string'
require 'bundler/setup'
require 'rspec/retry'
require 'require_all'
require 'json'
require 'pp'

Bundler.require :default
require 'active_support'
require 'active_support/core_ext'

require_rel './helpers'
require_rel './models'
require_rel '../utils'
require_rel '../utils/applications'

RSpec.configure do |config|
  config.filter_run_when_matching :focus
  config.default_formatter = 'doc'
  config.order = :random

  config.verbose_retry = true
  config.display_try_failure_messages = true
  config.example_status_persistence_file_path = 'examples.txt'

  ArbiterApi.new.run
  ArbiterUi.new.run

  puts ''
  # puts ColorizedString[read_file("../utils/pass-ascii.txt")].green
  # puts ColorizedString[read_file("../utils/fail-ascii.txt")].red
end

def post(url, request_body)
  HTTParty.post(
    "#{arbiter_api_url(url)}",
    :headers => {
      'Content-Type' => 'application/json'
    },
    :basic_auth => {
      username: 'admin',
      password: 'admin'
    },
    :body => request_body
  )
end
