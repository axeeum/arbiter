def read_fixture(path)
  file = File.open(File.join(File.dirname(__FILE__), "../fixtures/#{path}"), 'r')
  content = file.read
  file.close

  content
end