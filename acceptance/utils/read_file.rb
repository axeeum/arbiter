def read_file(path)
  file = File.open(File.join(File.dirname(__FILE__), "#{path}"), 'r')
  content = file.read
  file.close

  content
end