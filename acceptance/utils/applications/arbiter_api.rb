require_relative './urls'
require_relative './application'

class ArbiterApi < Application
  def initialize
    @name = 'arbiter-api'
    @dir = 'applications/arbiter-api'
    @base_url = arbiter_api_url('')
    @health_url = arbiter_api_url('/health')
    @port = arbiter_api_port
    @max_timeout_in_seconds = 30
  end
end
