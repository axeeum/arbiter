require_relative './urls'
require_relative './application'

class ArbiterUi < Application
  def initialize
    @name = 'arbiter-ui'
    @dir = 'applications/arbiter-ui'
    @base_url = arbiter_ui_url('')
    @health_url = arbiter_ui_url('/health')
    @port = arbiter_ui_port
    @max_timeout_in_seconds = 30
  end
end
