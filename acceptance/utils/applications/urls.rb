def arbiter_api_port
  9090
end

def arbiter_ui_port
  9091
end

def arbiter_api_url(path)
  "http://localhost:#{arbiter_api_port}#{path}"
end

def arbiter_ui_url(path)
  "http://localhost:#{arbiter_ui_port}#{path}"
end